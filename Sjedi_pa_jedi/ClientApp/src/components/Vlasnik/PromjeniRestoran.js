﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import FileBase64 from 'react-file-base64';



export class IzRestoran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restoran: ""
        };

        var oib = this.props.match.params.oib;

        fetch('api/Vlasnik/GetRestoran/' + oib)
            .then(response => response.json())
            .then(restorann => {
                this.setState({ restoran: restorann });
            });
       

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.ime = this.ime.bind(this);
        this.adresa = this.adresa.bind(this);
        this.telefon = this.telefon.bind(this);
        this.fax = this.fax.bind(this);
        this.iban = this.iban.bind(this);
        this.ziroRacun = this.ziroRacun.bind(this);

    }

    ime(event) {
        this.state.restoran.ime = event.target.value;
        this.setState(this.state.restoran);
    }
    adresa(event) {
        this.state.restoran.adresa = event.target.value;
        this.setState(this.state.restoran);
    }
    telefon(event) {
        this.state.restoran.telefon = event.target.value;
        this.setState(this.state.restoran);
    }
    fax(event) {
        this.state.restoran.fax = event.target.value;
        this.setState(this.state.restoran);
    }
    iban(event) {
        this.state.restoran.iban = event.target.value;
        this.setState(this.state.restoran);
    }
    ziroRacun(event) {
        this.state.restoran.ziroRacun = event.target.value;
        this.setState(this.state.restoran);
    }
    

    handleCancel() {
      
        this.props.history.push("/vlasnikRestorani" );
    }

    handleSubmit(event) {
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/PromjeniRestoran', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                this.props.history.push("/vlasnikRestorani");
            })
    }

    getFiles(files) {
        var slikaa = files;

        var slika = slikaa.base64;
        this.state.restoran.slika = slika;
        this.setState( this.state.restoran );

    }
    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label>OIB: </Label>
                                    <Label>{this.state.restoran.oib}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Ime: </Label>
                                    <Input type="text" value={this.state.restoran.ime} onChange={this.ime} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Adresa: </Label>
                                    <Input type="text" value={this.state.restoran.adresa} onChange={this.adresa} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Telefon: </Label>
                                    <Input type="text" value={this.state.restoran.telefon} onChange={this.telefon} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Fax: </Label>
                                    <Input type="text" value={this.state.restoran.fax} onChange={this.fax} />
                                </FormGroup>

                                

                                <FormGroup row>
                                    <Label>Iban: </Label>
                                    <Input type="text" value={this.state.restoran.iban} onChange={this.iban} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Žiro račun: </Label>
                                    <Input type="text" value={this.state.restoran.ziroRacun} onChange={this.ziroRacun} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="slika">Slika: &nbsp;</Label>
                                    <img src={this.state.restoran.slika} width="150" height="75" />&nbsp;
                                    <FileBase64 onDone={this.getFiles.bind(this)} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>



            </div>


        );
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class JeloKosaraDTO
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public decimal Cijena { get; set; }
        public string Slika { get; set; }
        public int Kolicina { get; set; }
        public int brojKartice { get; set; }
        public int OIBRestorana { get; set; }
    }
}

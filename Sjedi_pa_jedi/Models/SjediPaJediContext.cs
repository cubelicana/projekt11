﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SjediPaJedi.Models
{
    public partial class SjediPaJediContext : DbContext
    {
        public SjediPaJediContext()
        {
        }

        public SjediPaJediContext(DbContextOptions<SjediPaJediContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Jelo> Jelo { get; set; }
        public virtual DbSet<Kategorija> Kategorija { get; set; }
        public virtual DbSet<Korisnik> Korisnik { get; set; }
        public virtual DbSet<Narudzba> Narudzba { get; set; }
        public virtual DbSet<Osvrt> Osvrt { get; set; }
        public virtual DbSet<PripadaRestoranu> PripadaRestoranu { get; set; }
        public virtual DbSet<Restoran> Restoran { get; set; }
        public virtual DbSet<Rezervacija> Rezervacija { get; set; }
        public virtual DbSet<Stol> Stol { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=sjedipajedi.database.windows.net,1433;Initial Catalog=SjediPaJedi;Persist Security Info=False;User ID=rokom;Password=jjknwacd9.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Jelo>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Cijena).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Idkategorije).HasColumnName("IDKategorije");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Slika).IsUnicode(false);

                entity.HasOne(d => d.IdkategorijeNavigation)
                    .WithMany(p => p.Jelo)
                    .HasForeignKey(d => d.Idkategorije)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Jelo__IDKategori__59063A47");
            });

            modelBuilder.Entity<Kategorija>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Oibrestorana).HasColumnName("OIBRestorana");

                entity.HasOne(d => d.OibrestoranaNavigation)
                    .WithMany(p => p.Kategorija)
                    .HasForeignKey(d => d.Oibrestorana)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Kategorij__OIBRe__59FA5E80");
            });

            modelBuilder.Entity<Korisnik>(entity =>
            {
                entity.HasKey(e => e.KorisnickoIme)
                    .HasName("PK__Korisnik__992E6F93C3096F6A");

                entity.Property(e => e.KorisnickoIme)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BrojMobitela)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lozinka)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Narudzba>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Idjela).HasColumnName("IDJela");

                entity.Property(e => e.KorisnickoIme)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdjelaNavigation)
                    .WithMany(p => p.Narudzba)
                    .HasForeignKey(d => d.Idjela)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Narudzba__IDJela__5BE2A6F2");

                entity.HasOne(d => d.KorisnickoImeNavigation)
                    .WithMany(p => p.Narudzba)
                    .HasForeignKey(d => d.KorisnickoIme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Narudzba__Korisn__5AEE82B9");
            });

            modelBuilder.Entity<Osvrt>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.KorisnickoIme)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Odgovor)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Oibrestorana).HasColumnName("OIBRestorana");

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.KorisnickoImeNavigation)
                    .WithMany(p => p.Osvrt)
                    .HasForeignKey(d => d.KorisnickoIme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osvrt__Korisnick__5CD6CB2B");

                entity.HasOne(d => d.OibrestoranaNavigation)
                    .WithMany(p => p.Osvrt)
                    .HasForeignKey(d => d.Oibrestorana)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Osvrt__OIBRestor__5DCAEF64");
            });

            modelBuilder.Entity<PripadaRestoranu>(entity =>
            {
                entity.HasKey(e => new { e.KorisnickoIme, e.Oibrestorana })
                    .HasName("C_PripadaRestoranu");

                entity.Property(e => e.KorisnickoIme)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Oibrestorana).HasColumnName("OIBRestorana");
            });

            modelBuilder.Entity<Restoran>(entity =>
            {
                entity.HasKey(e => e.Oib)
                    .HasName("PK__Restoran__CB394B3F1F7F9A84");

                entity.Property(e => e.Oib)
                    .HasColumnName("OIB")
                    .ValueGeneratedNever();

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Iban)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Slika).IsUnicode(false);

                entity.Property(e => e.Telefon)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ZiroRacun)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rezervacija>(entity =>
            {
                entity.HasKey(e => e.BrojRezervacije)
                    .HasName("PK__Rezervac__2B3BC787BAED317A");

                entity.Property(e => e.BrojRezervacije).ValueGeneratedNever();

                entity.Property(e => e.KorisnickoIme)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Vrijeme).HasColumnType("datetime");

                entity.HasOne(d => d.KorisnickoImeNavigation)
                    .WithMany(p => p.Rezervacija)
                    .HasForeignKey(d => d.KorisnickoIme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rezervaci__Koris__5EBF139D");

                entity.HasOne(d => d.SifraStolaNavigation)
                    .WithMany(p => p.Rezervacija)
                    .HasForeignKey(d => d.SifraStola)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rezervaci__Sifra__5FB337D6");
            });

            modelBuilder.Entity<Stol>(entity =>
            {
                entity.HasKey(e => e.Sifra)
                    .HasName("PK__STol__32DBC78ECBE6719E");

                entity.ToTable("STol");

                entity.Property(e => e.Sifra).ValueGeneratedNever();

                entity.Property(e => e.Oibrestorana).HasColumnName("OIBRestorana");

                entity.HasOne(d => d.OibrestoranaNavigation)
                    .WithMany(p => p.Stol)
                    .HasForeignKey(d => d.Oibrestorana)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__STol__OIBRestora__60A75C0F");
            });
        }
    }
}

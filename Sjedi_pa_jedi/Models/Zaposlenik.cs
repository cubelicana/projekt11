﻿using SjediPaJedi.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models
{
    public class Zaposlenik : Korisnik
    {
        public SjediPaJediContext baza = new SjediPaJediContext();

        public List<NarudzbaDTO> pogledajAktivneNarudzbe(int OIBrestorana)
        {
            List<NarudzbaDTO> lista = new List<NarudzbaDTO>();
            var kategorije = baza.Kategorija.Where(k => k.Oibrestorana == OIBrestorana).ToList();
            foreach(var kategorija in kategorije)
            {
                var jela = baza.Jelo.Where(j => j.Idkategorije == kategorija.Id).ToList();
                foreach(var jelo in jela)
                {
                    if (baza.Narudzba.Any(n => n.Idjela == jelo.Id && n.Gotova == false))
                    {
                        var narudzbe = baza.Narudzba.Where(n => n.Idjela == jelo.Id && n.Gotova == false).ToList();
                        foreach (var narudzba in narudzbe)
                        {
                            NarudzbaDTO narudzbaDTO = new NarudzbaDTO();
                            narudzbaDTO.KorisnickoIme = narudzba.KorisnickoIme;
                            narudzbaDTO.jelo = jelo.Naziv;
                            narudzbaDTO.Id = narudzba.Id;
                            narudzbaDTO.Vrijeme = narudzba.Vrijeme;
                            lista.Add(narudzbaDTO);
                        }
                    }
                }
            }
            return lista;
        }

        public void oznaciNarudzbu(int IDnarudzbe)
        {
            var narudzba = baza.Narudzba.Where(n => n.Id == IDnarudzbe && n.Gotova==false).First();
           
            narudzba.Gotova = true;
            baza.SaveChanges();
            

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class RezervacijeKlijentaDTO
    {
        public int brojRezervacije { get; set; }
        public int kapacitet { get; set; }
        public int SifraStola { get; set; }
        public DateTime Vrijeme { get; set; }
        public string ImeRestorana { get; set; }

        
    }
}

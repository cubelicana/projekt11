﻿import React from 'react';
import { Table, Button, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';


export class Zaposlenici extends React.Component {
 
    constructor(props) {
        super(props);
        this.state = {
            zaposlenici: []
        };

        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/GetZaposlenike/'+oib)
            .then(response => response.json())
            .then(zaposlenici => {
                this.setState({ zaposlenici });
            });
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }

   





    handleDelete(kIme) {

        fetch('api/Vlasnik/DeleteZaposlenik/' + kIme, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    zaposlenici: this.state.zaposlenici.filter((rec) => {
                        return (rec.korisnickoIme != kIme);
                    })
                })

        })

    }

handleAdd() {
       var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikDodajZaposlenika/" + oib);
    }


    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col md={{ size: 12, offset: 1 }}>

                <Table>
                    <thead>
                        <tr>
                            <th>Korisnicko ime</th>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Email</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.zaposlenici.map(zaposlenik =>
                            <tr key={zaposlenik.korisnickoIme}>
                                <td>{zaposlenik.korisnickoIme}</td>
                                <td>{zaposlenik.ime}</td>
                                <td>{zaposlenik.prezime}</td>
                                <td>{zaposlenik.email}</td>
                                
                                <td>
                                    <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovog zaposlenika?')) this.handleDelete(zaposlenik.korisnickoIme) }}>Izbriši</Button>
                                </td>
                            </tr>

                        )}
                    </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button color="primary" id="Dodaj" onClick={this.handleAdd}>Dodaj</Button>
                        </Col>
                    </Row>
                </Container>
            </div >
        );
    }
}
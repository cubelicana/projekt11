﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';


export class VlasnikOsvrti extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            osvrti: []

        };

        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/GetOsvrte/'+oib)
            .then(response => response.json())
            .then(osvrti => {
                this.setState({ osvrti });
            });
        


    }



    odgovoriNaOsvrt(id) {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikOdgovorNaOsvrt/" + oib + "/" + id);
    }




    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            {this.state.osvrti.map(osvrt =>
                                <Card>
                                    <CardBody>

                                        <CardTitle>Korisnik: &nbsp;{osvrt.korisnickoIme}</CardTitle>
                                        <CardSubtitle>Ocjena:&nbsp;{osvrt.ocjena}</CardSubtitle>
                                        <CardText>Opis:&nbsp; {osvrt.opis}</CardText>
                                        <CardText>Odgovor:&nbsp; {osvrt.odgovor}</CardText>
                                        
                                        <Button color="primary" onClick={(kIme) => { this.odgovoriNaOsvrt(osvrt.id) }}>Odgovori</Button>
                                        


                                    </CardBody>
                                </Card>
                            )}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
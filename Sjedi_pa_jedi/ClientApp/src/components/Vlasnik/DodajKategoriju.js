﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';

export class VlasnikDodajKategoriju extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            naziv: '',
            OibRestorana: '',
            
        };
        
        this.naziv = this.naziv.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }

    componentDidMount() {
        var oib = this.props.match.params.oib;
        this.setState({ OibRestorana: oib });
    }

    
    naziv(event) {
        this.setState({ naziv: event.target.value });
    }


    napraviSubmit(event) {

        event.preventDefault();
        fetch('api/Vlasnik/PostKategorija', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/vlasnikKategorije/" + this.state.OibRestorana);
                else
                    alert('Greška! Nije moguće dodati ovu kategoriju');
            })
    }
    napraviCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikKategorije/" + oib);
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
            <VlasnikNavBar oibRestorana={oib} />
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <Form>

                            <FormGroup row>
                                <Label for="naziv">Naziv</Label>
                                <Input type="text" onChange={this.naziv} id="naziv" value={this.state.naziv} required />
                            </FormGroup>


                            <FormGroup>
                                    <Button color="primary" id="submit" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
                </Container>
            </div>
        );
    }
}
﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

export class IzRezervaciju extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            bR: '',
            rezervacija:'',
            restorani: [],
        };

        var bR = this.props.match.params.bR;
        fetch('api/Klijent/GetRezervaciju/'+bR)
            .then(response => response.json())
            .then(rezervacijaa => {
                this.setState({
                    rezervacija: rezervacijaa,
                    restorani: rezervacijaa.restorani,
                    bR:bR
                });
                
            });




        this.restoran = this.restoran.bind(this);
        this.datum = this.datum.bind(this);
        this.kapacitet = this.kapacitet.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }




    kapacitet(event) {
        this.state.rezervacija.kapacitet = event.target.value;
        this.setState(this.state.rezervacija);
    }

    napraviSubmit(event) {

        event.preventDefault();
        fetch('api/Klijent/IzmjeniRezervaciju', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/klijentRezervacije");
                else if (findresponse.result == 'neuspjesnoVrime')
                    alert('Greška! Nije moguće izvršiti rezervaciju za odabrano vrijeme');
                else if (findresponse.result == 'neuspjesnoKapacitet')
                    alert('Greška! Nije moguće izvršiti rezervaciju za odabrani kapacitet');
                else if (findresponse.result == 'neuspjesno')
                    alert('Greška! Vrijeme rezervacije more biti između 12PM i 9PM');
                else if (findresponse.result == 'datum')
                    alert('Greška! Rezervacije nisu moguće za datume prije današnjeg');
                else if (findresponse.result == 'prosljedi')
                    this.props.history.push("/klijentRezervacije");
            })




    }
    restoran(event) {
        var oib = event.target.value.split('|');
        var oibb = oib[1].trim();
        this.state.rezervacija.restoran = oibb;
        this.setState(this.state.rezervacija);
       
    }



    datum(event) {
        this.state.rezervacija.datum = event.target.value;
        this.setState(this.state.rezervacija);

    }
    napraviCancel() {

        this.props.history.push("/klijentRezervacije");
    }
    render() {
       
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label for="kapacitet">Kapacitet:&nbsp;</Label>
                                    <Input type="text" onChange={this.kapacitet} id="kapacitet" value={this.state.rezervacija.kapacitet} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Datum i vrijeme:&nbsp;</Label>
                                    <input type="datetime-local" onChange={this.datum} selected={this.state.rezervacija.datum} required />

                                </FormGroup>



                                <FormGroup row>
                                    <Label for="restoran">Restoran:&nbsp;</Label>
                                    <Input type="select" name="odaberi" id="restoran" onChange={this.restoran} selected={this.state.rezervacija.restoran}>
                                        {this.state.restorani.map(restoran =>
                                            <option key={restoran.oib} data-key={restoran.oib}>{restoran.ime}&nbsp;|&nbsp;{restoran.oib}</option>
                                        )}
                                    </Input>
                                </FormGroup>
                                <FormGroup row>
                                    <Button color="success" onClick={this.napraviSubmit}>Izmjeni</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


export class IzKategoriju extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            kategorija: ""
        };

        var id = this.props.match.params.id;

        fetch('api/Vlasnik/GetKategorija/' + id)
            .then(response => response.json())
            .then(kategorijaa => {
                this.setState({ kategorija: kategorijaa });
            });
        
        this.naziv = this.naziv.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }


    

    naziv(event) {
        this.state.kategorija.naziv = event.target.value;
        this.setState(this.state.kategorija);
    }

    handleCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikKategorije/" + oib);
    }

    handleSubmit(event) {
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/PromjeniKategoriju/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            this.props.history.push("/vlasnikKategorije/" + oib);
        });
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                              

                                <FormGroup row>
                                    <Label for="naziv">Naziv: </Label>
                                    <Input type="text" value={this.state.kategorija.naziv} onChange={this.naziv} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>



            </div>


        );
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SjediPaJedi.Models;
using Microsoft.EntityFrameworkCore;
using SjediPaJedi.Models.DTO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SjediPaJedi.Controllers
{
    
    public class HomeController : Controller
    {
        SjediPaJediContext baza = new SjediPaJediContext();
        Klijent klijent = new Klijent();

        [HttpPost]
        [Route("api/Home/registracija")]
        public IActionResult Registracija([FromBody]Korisnik noviKorisnik)
        {
            int rez = noviKorisnik.Registracija();
            if (rez == 1)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, noviKorisnik.KorisnickoIme));
                claims.Add(new Claim(ClaimTypes.Role, noviKorisnik.RazinaOvlasti.ToString()));

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal).Wait();
                return Json(new { result = "uspjesno" });
            }
            else
                return Json(new { result = "neuspjesno" });

        }

        [HttpPost]
        [Route("api/Home/prijava")]
        public IActionResult Prijava([FromBody]KlijentDTO prijavaKlijent)
        {
            int rez = klijent.prijaviSe(prijavaKlijent);
            if (rez != 0)
            {
                if (prijavaKlijent.Uloga == "Klijent")
                {
                    napraviCookie(prijavaKlijent, rez);
                    return Json(new { result = "Klijent" });
                }
                else if (prijavaKlijent.Uloga == "Zaposlenik" && rez == 2)
                {
                    napraviCookie(prijavaKlijent, rez);
                    return Json(new { result = "Zaposlenik" });
                }
                else if (prijavaKlijent.Uloga == "Vlasnik" && rez == 3)
                {
                    napraviCookie(prijavaKlijent, rez);
                    return Json(new { result = "Vlasnik" });
                }
                else if (prijavaKlijent.Uloga == "Administrator" && rez == 4)
                {
                    napraviCookie(prijavaKlijent, rez);
                    return Json(new { result = "Administrator" });
                }
                else
                    return Json(new { result = "nemaOvlasti" });


                
            }
            else
                return Json(new { result = "neuspjesno" });
        }

        public void napraviCookie(KlijentDTO klijent, int rez)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, klijent.KorisnickoIme));
            claims.Add(new Claim(ClaimTypes.Role, rez.ToString()));

            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal).Wait();
        }

        [HttpGet]
        [Route("api/Home/Restoran/{oib}")]
        public IActionResult Restoran(int oib)
        {
            RestoranDTO restoranDTO = new RestoranDTO();
            List<KategorijaDTO> kategorijeDTO = new List<KategorijaDTO>();
            
            using (var baza = new SjediPaJediContext())
            {
                var restoran = baza.Restoran.Where(r => r.Oib == oib)
                    .FirstOrDefault();
                restoranDTO.Adresa = restoran.Adresa;
                restoranDTO.Fax = restoran.Fax;
                restoranDTO.Iban = restoran.Iban;
                restoranDTO.Ime = restoran.Ime;
                restoranDTO.Oib = restoran.Oib;
                restoranDTO.Telefon = restoran.Telefon;
                restoranDTO.ZiroRacun = restoran.ZiroRacun;
                restoranDTO.Slika = restoran.Slika;

                var kategorije = baza.Kategorija.Where(r => r.Oibrestorana == oib).ToList();
                foreach(var kategorija in kategorije)
                {
                    List<JeloDTO> jelaDTO = new List<JeloDTO>();
                    KategorijaDTO kategorijaDTO = new KategorijaDTO();
                    kategorijaDTO.Naziv = kategorija.Naziv;
                    var jela = baza.Jelo.Where(r => r.Idkategorije == kategorija.Id);
                    foreach(var jelo in jela)
                    {
                        JeloDTO jeloDTO = new JeloDTO();
                        jeloDTO.Id = jelo.Id;
                        jeloDTO.Cijena = jelo.Cijena;
                        jeloDTO.Naziv = jelo.Naziv;
                        jeloDTO.Opis = jelo.Opis;
                        jeloDTO.Slika = jelo.Slika;
                        jelaDTO.Add(jeloDTO);
                    }
                    kategorijaDTO.Jelo = jelaDTO;
                    kategorijeDTO.Add(kategorijaDTO);


                }
                restoranDTO.kategorije = kategorijeDTO;

                var osvrt = baza.Osvrt.Where(r => r.Oibrestorana == oib).FirstOrDefault();
                if (osvrt != null)
                {
                    OsvrtDTO osvrtDTO = new OsvrtDTO();
                    osvrtDTO.KorisnickoIme = osvrt.KorisnickoIme;
                    osvrtDTO.Ocjena = osvrt.Ocjena;
                    osvrtDTO.Odgovor = osvrt.Odgovor;
                    osvrtDTO.Opis = osvrt.Opis;
                    restoranDTO.osvrt = osvrtDTO;
                }
                return Ok(restoranDTO);
            }
        }
        
    }

}


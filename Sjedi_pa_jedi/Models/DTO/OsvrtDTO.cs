﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class OsvrtDTO
    {
        
        public int Ocjena { get; set; }
        public string Opis { get; set; }
        public string KorisnickoIme { get; set; }
        public string Odgovor { get; set; }
    }
}

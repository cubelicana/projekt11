﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

export class VlasnikDodajStol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sifra: '',
            oibRestorana:'',
            kapacitet: '',
            
        };

        this.sifra = this.sifra.bind(this);
        this.kapacitet = this.kapacitet.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }
    componentDidMount() {
        var oib = this.props.match.params.oib;

        this.setState({ oibRestorana: oib });
    }

    sifra(event) {
        this.setState({ sifra: event.target.value });
    }
    kapacitet(event) {
        this.setState({ kapacitet: event.target.value });
    }
   
    napraviSubmit(event) {
        event.preventDefault();
        fetch('api/Vlasnik/PostStol', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/vlasnikStolovi/" + this.state.oibRestorana);
                else
                    alert('Greška! Šifra stola već postoji');
            })




    }

    napraviCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikStolovi/" + oib);
    }
    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>

                                
                                <FormGroup row>
                                    <Label for="sifra">Sifra stola</Label>
                                    <Input type="text" onChange={this.sifra} id="sifra" value={this.state.sifra} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="kapacitet">Kapacitet</Label>
                                    <Input type="text" onChange={this.kapacitet} id="kapacitet" value={this.state.kapacitet} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="primary" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
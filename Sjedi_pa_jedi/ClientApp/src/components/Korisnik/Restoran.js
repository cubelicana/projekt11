﻿import React from 'react';
import { Card, CardTitle,CardImg, CardText,Button, CardBody, ListGroupItemText, ListGroupItemHeading, CardSubtitle, ListGroup, ListGroupItem, Container, Row, Col } from 'reactstrap';

export class Restoran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restoran: [],
            kategorije: [],
            osvrt: "",
           



        };
      
        var oib = this.props.match.params.oib;
        fetch("api/Home/Restoran/" + oib)
            .then(response => response.json())
            .then(restorann => {
                if (restorann.osvrt == null)
                    this.setState({
                        restoran: restorann,
                        kategorije: restorann.kategorije,
                       
                    });
                else
                this.setState({
                    restoran: restorann,
                    kategorije: restorann.kategorije,
                    osvrt: restorann.osvrt,
                    
                    });
               
            });
        
        this.naPrijavu = this.naPrijavu.bind(this);
        this.naRegistraciju = this.naRegistraciju.bind(this);
       
        
    }
  
    naPrijavu() {
        this.props.history.push("/");
    }

    naRegistraciju() {
        this.props.history.push("/registracija");
    }

    renderOsvrt() {
        if (this.state.osvrt!='')
        return (
                <Card>
                    <CardBody>
                        <CardTitle>{this.state.osvrt.korisnickoIme}</CardTitle>
                        <CardSubtitle>Ocjena:&nbsp;{this.state.osvrt.ocjena}</CardSubtitle>
                        <CardText>Opis:&nbsp;{this.state.osvrt.opis}<br />Odgovor:&nbsp;{this.state.osvrt.odgovor}</CardText>
                    </CardBody>
                </Card>
                
            );
    }

    renderSlika(slika) {
        if (slika == null)
            return (<span/>);
        else
            return (<img src={slika} width="75" height="75" />);
    }
    render() {
        
        return (
            
            <div>
                
                <Container>
                    <Row>
                        <Col xs="3">
                            <CardImg src={this.state.restoran.slika} width="100%"/>
                            <Card body inverse color="info">
                                
                                <CardBody>
                                    
                                    <CardTitle>Naziv:&nbsp;{this.state.restoran.ime}</CardTitle>
                                    <CardSubtitle>Adresa:&nbsp;{this.state.restoran.adresa}</CardSubtitle>
                                    <CardText>OIB:&nbsp;{this.state.restoran.oib}<br/>Telefon:&nbsp;{this.state.restoran.telefon}<br/>Iban:&nbsp;{this.state.restoran.iban}
                                        <br/>Fax:&nbsp;{this.state.restoran.fax}<br/>Ziro racun:&nbsp;{this.state.restoran.ziroRacun}<br/></CardText>
                                </CardBody>
                            </Card>
                            
                        </Col>
                        <Col>
                            {this.state.kategorije.map(kategorija => 
                                <ListGroup>
                                    <ListGroupItem active>{kategorija.naziv}</ListGroupItem>
                                    {kategorija.jelo.map(jelo => 
                                        <ListGroupItem>
                                            <ListGroupItemHeading>{jelo.naziv}</ListGroupItemHeading>
                                            <ListGroupItemText>{this.renderSlika(jelo.slika)}&nbsp;<b>Opis:</b>&nbsp;{jelo.opis}&nbsp;<b>Cijena:</b>&nbsp;{jelo.cijena}</ListGroupItemText>
                                        </ListGroupItem>
                                        )}
                                </ListGroup>
                                )}
                                
                        </Col>
                        <Col xs="3">
                            {this.renderOsvrt()}
                            <Button color="primary" onClick={this.naPrijavu}>Prijava</Button>&nbsp;
                            <Button color="warning" onClick={this.naRegistraciju}>Registracija</Button>

                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}

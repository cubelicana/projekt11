﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Rezervacija
    {
        public int BrojRezervacije { get; set; }
        public int SifraStola { get; set; }
        public DateTime Vrijeme { get; set; }
        public string KorisnickoIme { get; set; }

        public virtual Korisnik KorisnickoImeNavigation { get; set; }
        public virtual Stol SifraStolaNavigation { get; set; }
    }
}

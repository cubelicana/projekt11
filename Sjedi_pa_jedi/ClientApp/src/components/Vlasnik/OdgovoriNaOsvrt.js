﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


export class OdgovoriNaOsvrt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            osvrt: ""
        };

        var id = this.props.match.params.id;

        fetch('api/Vlasnik/GetOsvrt/' + id)
            .then(response => response.json())
            .then(osvrtt => {
                this.setState({ osvrt: osvrtt });
            });
        this.odgovor = this.odgovor.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }


    odgovor(event) {
        this.state.osvrt.odgovor = event.target.value;
        this.setState(this.state.osvrt);
    }

    handleCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikOsvrti/" + oib);
    }

    handleSubmit(event) {
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/OdgovorOsvrta', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            this.props.history.push("/vlasnikOsvrti/" + oib);
        });
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label>Korisnik: &nbsp;</Label>
                                    <Label>{this.state.osvrt.korisnik}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="Ocjena">Ocjena:&nbsp; </Label>
                                    <Label>{this.state.osvrt.ocjena}</Label>                                  
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="Opis">Opis:&nbsp; </Label>
                                    <Label>{this.state.osvrt.opis}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="Odgovor">Odgovor:&nbsp; </Label>
                                    <Input type="text" value={this.state.osvrt.odgovor} onChange={this.odgovor} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>


               
            </div>


        );
    }
}
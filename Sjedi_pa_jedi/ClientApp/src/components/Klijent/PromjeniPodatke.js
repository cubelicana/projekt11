﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


export class IzPodatke extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            podaci: ""
        };

        

        fetch('api/Klijent/GetPodatci')
            .then(response => response.json())
            .then(podatcii => {
                this.setState({ podaci: podatcii });
            });

        this.kIme = this.kIme.bind(this);
        this.ime = this.ime.bind(this);
        this.prezime = this.prezime.bind(this);
        this.lozinka = this.lozinka.bind(this);
        this.bMoba = this.bMoba.bind(this);
        this.email = this.email.bind(this);
        this.bKartice = this.bKartice.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);

    }


    kIme(event) {
        this.state.podaci.korisnickoIme = event.target.value;
        this.setState(this.state.podaci);
    }
    ime(event) {
        this.state.podaci.ime = event.target.value;
        this.setState(this.state.podaci);
    }
    prezime(event) {
        this.state.podaci.prezime = event.target.value;
        this.setState(this.state.podaci);
    }
    lozinka(event) {
        this.state.podaci.lozinka = event.target.value;
        this.setState(this.state.podaci);
    }
    bMoba(event) {
        this.state.podaci.brojMobitela = event.target.value;
        this.setState(this.state.podaci);
    }
    email(event) {
        this.state.podaci.email = event.target.value;
        this.setState(this.state.podaci);
    }
    bKartice(event) {
        this.state.podaci.brojKartice = event.target.value;
        this.setState(this.state.podaci);
    }


    handleCancel() {
        
        this.props.history.push("/osobniPodatci");
    }

    handleSubmit(event) {
        
        fetch('api/Klijent/PodatciIzmjene', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno' || findresponse.result == 'nije minjano')
                    this.props.history.push("/osobniPodatci");
                else if(findresponse.result == 'vec postoji')
                    alert('Greška! Korisničko ime već postoji');
            })
    }

    render() {
     
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label>Korisničko ime: </Label>
                                    <Input type="text" value={this.state.podaci.korisnickoIme} onChange={this.kIme} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Lozinka: </Label>
                                    <Input type="text" value={this.state.podaci.lozinka} onChange={this.lozinka} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Ime: </Label>
                                    <Input type="text" value={this.state.podaci.ime} onChange={this.ime} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Prezime: </Label>
                                    <Input type="text" value={this.state.podaci.prezime} onChange={this.prezime} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Email: </Label>
                                    <Input type="text" value={this.state.podaci.email} onChange={this.email} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Broj mobitela: </Label>
                                    <Input type="text" value={this.state.podaci.brojMobitela} onChange={this.bMoba} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Broj kartice: </Label>
                                    <Input type="text" value={this.state.podaci.brojKartice} onChange={this.bKartice} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>



            </div>


        );
    }
}
﻿using SjediPaJedi.Models.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace SjediPaJedi.Models
{
    [NotMapped]
    public class Klijent : Korisnik
    {
        public SjediPaJediContext baza = new SjediPaJediContext();
        public int prijaviSe(KlijentDTO klijent)
        {
            using (var baza = new SjediPaJediContext())
            {
                if (baza.Korisnik.Any(o => o.KorisnickoIme == klijent.KorisnickoIme && o.Lozinka == klijent.Lozinka))
                {
                    return baza.Korisnik.Where(o => o.KorisnickoIme == klijent.KorisnickoIme && o.Lozinka==klijent.Lozinka).Select(o=>o.RazinaOvlasti).First();
                }
                return 0;
            }
        }

        public KorisnikDTO pregledajOsobnePodatke(string kIme)
        {
            using (var baza = new SjediPaJediContext())
            {
                var korisnickiPodaci = baza.Korisnik.Where(r => r.KorisnickoIme == kIme).First();
                KorisnikDTO korisnikDTO = new KorisnikDTO();
                korisnikDTO.BrojKartice = korisnickiPodaci.BrojKartice;
                korisnikDTO.BrojMobitela = korisnickiPodaci.BrojMobitela;
                korisnikDTO.Email = korisnickiPodaci.Email;
                korisnikDTO.Ime = korisnickiPodaci.Ime;
                korisnikDTO.KorisnickoIme = korisnickiPodaci.KorisnickoIme;
                korisnikDTO.Lozinka = korisnickiPodaci.Lozinka;
                korisnikDTO.Prezime = korisnickiPodaci.Prezime;
                korisnikDTO.RazinaOvlasti = korisnickiPodaci.RazinaOvlasti;
                return korisnikDTO;
            }
        }

        public int promijeniOsobnePodatke(Korisnik korisnik, string ime)
        {
            if (korisnik.KorisnickoIme == null)
                return 0;
            if (korisnik.KorisnickoIme != ime)
            {
                var korisnikStari = baza.Korisnik.SingleOrDefault(k => k.KorisnickoIme == ime);

                if (baza.Korisnik.Any(k => k.KorisnickoIme == korisnik.KorisnickoIme))
                    return 2;
                korisnikStari.KorisnickoIme = korisnik.KorisnickoIme;



                if (korisnikStari.Ime != korisnik.Ime)
                    korisnikStari.Ime = korisnik.Ime;

                if (korisnikStari.Prezime != korisnik.Prezime)
                    korisnikStari.Prezime = korisnik.Prezime;

                if (korisnikStari.BrojMobitela != korisnik.BrojMobitela)
                    korisnikStari.BrojMobitela = korisnik.BrojMobitela;

                if (korisnikStari.Email != korisnik.Email)
                    korisnikStari.Email = korisnik.Email;

                if (korisnikStari.BrojKartice != korisnik.BrojKartice)
                    korisnikStari.BrojKartice = korisnik.BrojKartice;

                if (korisnikStari.Lozinka != korisnik.Lozinka)
                    korisnikStari.Lozinka = korisnik.Lozinka;
            }



            baza.SaveChanges();
            return 1;
        }

        public void obrisiRacun(string kIme)
        {
            var korisnik = baza.Korisnik.SingleOrDefault(korisnikk => korisnikk.KorisnickoIme == kIme);
            if (baza.PripadaRestoranu.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var pR = baza.PripadaRestoranu.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var p in pR)
                {
                    baza.Remove(p);
                }
            }
            if (baza.Osvrt.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var osvrti = baza.Osvrt.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var osvrt in osvrti)
                {
                    baza.Remove(osvrt);
                }
            }
            if (baza.Narudzba.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var narudzbe = baza.Narudzba.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var narudzba in narudzbe)
                {
                    baza.Remove(narudzba);
                }
            }
            if (baza.Rezervacija.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var rezervacije = baza.Rezervacija.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var rezervacija in rezervacije)
                {
                    baza.Remove(rezervacija);
                }
            }
            baza.Remove(korisnik);
            baza.SaveChanges();
        }

        public int rezervirajStol(RezervacijaStolaDTO novaRezervacija, string korisnickoIme)
        {
            Rezervacija rezervacija = new Rezervacija();
            var restoran = baza.Restoran.Where(r => r.Oib == novaRezervacija.Restoran).First();

            if (baza.Rezervacija.Any())
            {
                var rez = baza.Rezervacija.Last();
                rezervacija.BrojRezervacije = rez.BrojRezervacije + 1;
            }
            else
                rezervacija.BrojRezervacije = 1;

            rezervacija.KorisnickoIme = korisnickoIme;
            DateTime vrime = Convert.ToDateTime(novaRezervacija.Datum);
            DateTime danas = DateTime.Today;
            if (vrime.Date < danas)
                return 5;
            TimeSpan devetUri = new TimeSpan(21, 0, 0);
            TimeSpan podne = new TimeSpan(12, 0, 0);
            if (vrime.TimeOfDay > devetUri || vrime.TimeOfDay < podne)
                return 4;
            TimeSpan interval = new TimeSpan(1, 0, 0);
            TimeSpan interval1 = new TimeSpan(1, 0, 0, 0);

            var stolovi = baza.Stol.Where(r => r.Oibrestorana == restoran.Oib).ToList();
            foreach(var stol in stolovi)
            {
                if(stol.Kapacitet >= novaRezervacija.Kapacitet)
                {
                    var rezervacijaStola = baza.Rezervacija.Where(r => r.SifraStola == stol.Sifra).ToList();
                    foreach(var rezer in rezervacijaStola)
                    {
                        if (rezer.Vrijeme.Date == vrime.Date)
                        {
                            DateTime plusUra = rezer.Vrijeme.AddHours(2);
                            DateTime minusUra = rezer.Vrijeme.AddHours(-2);
                            if (vrime > minusUra && vrime < plusUra)
                                return 3;
                        }
                        
                    }
                    rezervacija.Vrijeme = vrime;
                    rezervacija.SifraStola = stol.Sifra;
                    baza.Rezervacija.Add(rezervacija);
                    baza.SaveChanges();
                    return 1;
                }
            }
            return 2;
            
            
        }

        public int urediRezervaciju(IzmjenaRezervacijePost rezervacija)
        {
            if (rezervacija.Restoran == 0)
                return 0;
            
            var rezervacijaa = baza.Rezervacija.SingleOrDefault(r => r.BrojRezervacije == rezervacija.bR);
            DateTime vrime = Convert.ToDateTime(rezervacija.Datum);
           
            
                DateTime danas = DateTime.Today;
                if (vrime.Date < danas)
                    return 5;
                TimeSpan devetUri = new TimeSpan(21, 0, 0);
                TimeSpan podne = new TimeSpan(12, 0, 0);
                if (vrime.TimeOfDay > devetUri || vrime.TimeOfDay < podne)
                    return 4;
                TimeSpan interval = new TimeSpan(1, 0, 0);
                TimeSpan interval1 = new TimeSpan(1, 0, 0, 0);
                var restoran = baza.Restoran.Where(r => r.Oib == rezervacija.Restoran).First();
                var stolovi = baza.Stol.Where(r => r.Oibrestorana == restoran.Oib).ToList();
                foreach (var stol in stolovi)
                {
                    if (stol.Kapacitet >= rezervacija.Kapacitet)
                    {
                        var rezervacijaStola = baza.Rezervacija.Where(r => r.SifraStola == stol.Sifra).ToList();
                        foreach (var rezer in rezervacijaStola)
                        {
                            if (rezer.Vrijeme.Date == vrime.Date)
                            {
                                DateTime plusUra = rezer.Vrijeme.AddHours(2);
                                DateTime minusUra = rezer.Vrijeme.AddHours(-2);
                                if (vrime > minusUra && vrime < plusUra)
                                    return 3;
                            }

                        }
                        rezervacijaa.Vrijeme = vrime;
                        rezervacijaa.SifraStola = stol.Sifra;
                        
                        baza.SaveChanges();
                        return 1;
                    }
                }
            return 2;
            
            



        }


        public void obrisiRezervaciju(int idrezervacije)
        {
            var rezervacijab = baza.Rezervacija.SingleOrDefault(r => r.BrojRezervacije == idrezervacije);
            baza.Remove(rezervacijab);
            baza.SaveChanges();
        }

        public List<Osvrt> pogledajRecenzije(Restoran restoran)
        {
            var podatci = baza.Osvrt.Where(o => o.Oibrestorana == restoran.Oib).ToList();
            return (podatci);
        }

        
        public void platiNarudzbu(List<JeloKosaraDTO> jela, string kIme)
        {
            foreach (var jelo in jela)
            {
                Narudzba narudzba = new Narudzba();
                if (!baza.Narudzba.Any())
                {
                    narudzba.Id = 1;
                }
                else
                {
                    var narudzbaa = baza.Narudzba.Last();
                    narudzba.Id = narudzbaa.Id + 1;
                }
                narudzba.Idjela = jelo.Id;
                narudzba.KorisnickoIme = kIme;
                DateTime sad = DateTime.Now;
                TimeSpan sada = sad.TimeOfDay;
                narudzba.Vrijeme = sada;
                narudzba.Gotova = false;
                baza.Narudzba.Add(narudzba);
            }
            baza.SaveChanges();
        }

        public void ocijeniRestoran(OsvrtKlijentDTO noviOsvrt, int oib, String kIme)
        {
            Osvrt osvrt = new Osvrt();
            osvrt.Oibrestorana = oib;
            osvrt.KorisnickoIme = kIme;
            osvrt.Opis = noviOsvrt.komentar;
            osvrt.Ocjena = noviOsvrt.ocjena;
            osvrt.Odgovor = "";
            if (baza.Osvrt.Any())
            {
                var osvrtt = baza.Osvrt.Last();
                osvrt.Id = osvrtt.Id + 1;
            }
            else
                osvrt.Id = 1;
            baza.Osvrt.Add(osvrt);
            baza.SaveChanges();
            return;
            
        }
    }
}

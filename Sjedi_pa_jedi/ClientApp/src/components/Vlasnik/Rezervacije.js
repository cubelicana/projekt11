﻿import React from 'react';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';


export class VlasnikRezervacije extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rezervacije: []

        };
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/GetRezervacije/' + oib)
            .then(response => response.json())
            .then(rezervacijee => {
                if (rezervacijee.result == 'nema')
                    this.setState({ rezervacije: [] });
                else
                    this.setState({ rezervacije:rezervacijee });
            });
    }



    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            {this.state.rezervacije.map(rezervacija =>
                                <Card key={rezervacija.brojRezervacije}>
                                    <CardBody>

                                        <CardTitle>Korisnik: &nbsp;{rezervacija.korisnickoIme}</CardTitle>
                                        <CardSubtitle>Vrijeme:&nbsp;{rezervacija.vrijeme}</CardSubtitle>
                                        <CardText>Šifra stola:&nbsp; {rezervacija.sifraStola}</CardText>
                                        <CardText>Broj rezervacije:&nbsp;{rezervacija.brojRezervacije}</CardText>
                                        
                                    </CardBody>
                                </Card>
                            )}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
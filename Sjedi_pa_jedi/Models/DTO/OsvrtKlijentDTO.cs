﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class OsvrtKlijentDTO
    {
        public int ocjena { get; set; }
        public string komentar { get; set; }
    }
}

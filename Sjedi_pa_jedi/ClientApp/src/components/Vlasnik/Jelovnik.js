﻿import React from 'react';
import { Card, CardTitle, CardText, Button, CardBody, ListGroupItemText, ListGroupItemHeading, CardSubtitle, ListGroup, ListGroupItem, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';

export class VlasnikJelovnik extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            kategorije: [],
            jelo:[]
        };

        var oib = this.props.match.params.oib;
        fetch("api/Vlasnik/GetKategorijeSJelima/" + oib)
            .then(response => response.json())
            .then(kategorijee => {
                this.setState({
                    kategorije: kategorijee,
                    jelo:kategorijee.jelo
                    

                });
            });

        this.dodajProizvoid = this.dodajProizvoid.bind(this);
        this.promjeniJelo = this.promjeniJelo.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.dobavi = this.dobavi.bind(this);
        
    }


    dodajProizvoid(id) {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikDodajProizvod/" + oib + "/" + id);
    }
    promjeniJelo(id) {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikIzProizvod/" + oib + "/" + id);
    }
   
    handleDelete(id) {
        fetch('api/Vlasnik/DeleteJelo/' + id, {
            method: 'delete'
        }).then(data => {
            this.dobavi();

        })
    }
    dobavi() {
        var oib = this.props.match.params.oib;
        fetch("api/Vlasnik/GetKategorijeSJelima/" + oib)
            .then(response => response.json())
            .then(kategorijee => {
                this.setState({
                    kategorije: kategorijee,
                    jelo: kategorijee.jelo


                });
            });
    }
    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>  
                       
                        <Col md={{ size: 10, offset: 1 }}>
                            {this.state.kategorije.map(kategorija =>
                                <ListGroup>
                                    <ListGroupItem active>{kategorija.naziv}&nbsp;<Button color="warning" onClick={(a)=>this.dodajProizvoid(kategorija.id)}>Dodaj jelo</Button></ListGroupItem>
                                    {kategorija.jelo.map(jelo =>
                                        <ListGroupItem>
                                            <ListGroupItemHeading>{jelo.naziv}&nbsp;<Button color="success" onClick={(a) => this.promjeniJelo(jelo.id)}>Promjeni</Button>&nbsp;<Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovo jelo?')) this.handleDelete(jelo.id) }}>Izbriši</Button></ListGroupItemHeading>
                                            
                                            <ListGroupItemText>{jelo.opis}&nbsp;&nbsp;Cijena:&nbsp;{jelo.cijena}</ListGroupItemText>
                                        </ListGroupItem>
                                    )}
                                </ListGroup>
                            )}

                        </Col>
                     
                    </Row>
                </Container>
            </div>

        );
    }
}
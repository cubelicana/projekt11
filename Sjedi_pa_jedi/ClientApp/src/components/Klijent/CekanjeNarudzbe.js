﻿import React from 'react';

import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Jumbotron } from 'reactstrap';


export class CekanjeNarudzbe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gotova:false,
            oib:props.match.params.oib
        };

        
        this.tick = this.tick.bind(this);
      
    }
    tick() {
        
        fetch('api/Klijent/Cekanje')
            .then(response => response.json())
            .then(rezultat => {
                if (rezultat.result == 'gotovo')
                    this.props.history.push("/klijentNapisiOsvrt/" + this.state.oib);
                
                    
            });
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 2000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

  

    render() {

        return (
            <div>
               
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Jumbotron>
                                <h2>Hvala na narudžbi</h2>
                                <p>Čekanje na potvrdu...</p><br />
                                <p>Ostanite na ovoj stranici sve dok potvrda ne stigne</p>
                             </Jumbotron>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
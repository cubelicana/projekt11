﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Stol
    {
        public Stol()
        {
            Rezervacija = new HashSet<Rezervacija>();
        }

        public int Sifra { get; set; }
        public int Oibrestorana { get; set; }
        public int Kapacitet { get; set; }

        public virtual Restoran OibrestoranaNavigation { get; set; }
        public virtual ICollection<Rezervacija> Rezervacija { get; set; }
    }
}

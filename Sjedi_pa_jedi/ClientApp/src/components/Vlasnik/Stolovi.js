﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Table, Button, Container, Row, Col } from 'reactstrap';




export class VlasnikStolovi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stolovi: []
        };
        var oib = this.props.match.params.oib;

        fetch('api/Vlasnik/GetStolove/' + oib)
            .then(response => response.json())
            .then(stolovi => {
                this.setState({ stolovi });
            });
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
       
    }



    handleDelete(sifra) {

        fetch('api/Vlasnik/DeleteStol/' + sifra, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    stolovi: this.state.stolovi.filter((rec) => {
                        return (rec.sifra != sifra);
                    })
                })

        })

    }

    handleAdd() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikNoviStol/"+oib);
    }

    odaberiStol(sifra) {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikIzStol/"+oib+"/"+sifra);
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 12, offset: 2 }}>
            
                <Table>
                    <thead>
                        <tr>
                            <th>Sifra</th>
                            <th>Kapacitet</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.stolovi.map(stol =>
                            <tr key={stol.sifra}>
                                <td>{stol.sifra}</td>
                                <td>{stol.kapacitet}</td>
                                <td>
                                    <Button color="warning" onClick={(kime) => { this.odaberiStol(stol.sifra) }}>Promjeni</Button>
                                    &nbsp;
                                    <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovaj stol?')) this.handleDelete(stol.sifra) }}>Izbriši</Button>
                                </td>
                            </tr>

                        )}
                    </tbody>
                </Table>
                
                    <Row>
                             <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button color="primary" id="Dodaj" onClick={this.handleAdd}>Dodaj</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
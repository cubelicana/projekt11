﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Osvrt
    {
        public int Id { get; set; }
        public int Ocjena { get; set; }
        public string Opis { get; set; }
        public string KorisnickoIme { get; set; }
        public int Oibrestorana { get; set; }
        public string Odgovor { get; set; }

        public virtual Korisnik KorisnickoImeNavigation { get; set; }
        public virtual Restoran OibrestoranaNavigation { get; set; }
    }
}

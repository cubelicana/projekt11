﻿import React from 'react';
import { ZaposlenikNavBar } from './ZaposlenikNavBar';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';



export class PogledajAktivneNarudzbe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            narudzbe: []

        };

       
        this.tick = this.tick.bind(this);
        this.renderNarudzbe = this.renderNarudzbe.bind(this);
    }
    tick() {

        fetch('api/Zaposlenik/GetNarudzbe')
            .then(response => response.json())
            .then(narudzbee => {
                if (narudzbee.result == 'Nema')
                    this.setState({ narudzbe: null })
                else
                    this.setState({ narudzbe: narudzbee })
            });
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 2000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    oznaciGotovo(id) {
        fetch('api/Zaposlenik/OznaciNarudzbu', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(id)
        }).then(data => {
            this.setState(
                {
                    narudzbe: this.state.narudzbe.filter((rec) => {
                        return (rec.id != id);
                    })
                })

        })
    }

    renderNarudzbe() {
        if (this.state.narudzbe == null)
            return (<h2>Nema aktivnih narudzbi</h2>);
        else
            return (
                this.state.narudzbe.map(narudzba =>
                    <Card>
                        <CardBody>

                            <CardTitle>ID:&nbsp; {narudzba.id}</CardTitle>
                            <CardSubtitle>Vrijeme:&nbsp; {narudzba.vrijeme}</CardSubtitle>
                            <CardText>Jelo:&nbsp;{narudzba.jelo}&nbsp;Korisnik:&nbsp;{narudzba.korisnickoIme}</CardText>
                            <Button color="primary" onClick={() => this.oznaciGotovo(narudzba.id) }>Gotova</Button>


                        </CardBody>
                    </Card>));

    }



    render() {
        return (
            <div>
                <ZaposlenikNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            {this.renderNarudzbe()}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
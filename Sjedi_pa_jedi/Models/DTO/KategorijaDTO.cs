﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class KategorijaDTO
    {
        
        public string Naziv { get; set; }
       
        public virtual List<JeloDTO> Jelo { get; set; }
    }
}

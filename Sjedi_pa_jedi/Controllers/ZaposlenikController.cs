﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SjediPaJedi.Models;

namespace SjediPaJedi.Controllers
{
    [ApiController]
    public class ZaposlenikController : Controller
    {
        Zaposlenik zaposlenik = new Zaposlenik();

        [HttpGet]
        [Route("api/Zaposlenik/GetNarudzbe")]
        public IActionResult GetNarudzbe()
        {
            using (var baza = new SjediPaJediContext())
            {
                var ime = HttpContext.User.Identity.Name;
                var restoran = baza.PripadaRestoranu.Where(pR => pR.KorisnickoIme == ime).First();
                var narudzbe = zaposlenik.pogledajAktivneNarudzbe(restoran.Oibrestorana);
                if (narudzbe.Count==0)
                    return Json(new { result = "Nema" });
                else
                    return Ok(narudzbe);
            }
        }

        [HttpPost]
        [Route("api/Zaposlenik/OznaciNarudzbu")]
        public IActionResult OznaciNarudzbu([FromBody]int id)
        {
            zaposlenik.oznaciNarudzbu(id);
            
                return Ok();
         
            
                
            
        }
    }
}

﻿import React from 'react';

import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { KlijentNavBar } from './NavBarBezRestorana';

export class RezervirajStol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
            datum: new Date(),
            kapacitet: '',         
            restoran: '',
            restorani: [],

        };


        fetch('api/Klijent/GetRestorane')
            .then(response => response.json())
            .then(restoranii => {
                this.setState({ restorani: restoranii });
                var oib = restoranii[0].oib;
                this.setState({ restoran: oib });
            });

        
        

        this.restoran = this.restoran.bind(this);
        this.datum = this.datum.bind(this);
        this.kapacitet = this.kapacitet.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }

   
    
   
    kapacitet(event) {
        this.setState({ kapacitet: event.target.value });
    }

    napraviSubmit(event) {
        
        event.preventDefault();
        fetch('api/Klijent/PostRezervaciju', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/klijentRezervacije");
                else if (findresponse.result == 'neuspjesnoVrime')
                    alert('Greška! Nije moguće izvršiti rezervaciju za odabrano vrijeme');
                else if (findresponse.result == 'neuspjesnoKapacitet')
                    alert('Greška! Nije moguće izvršiti rezervaciju za odabrani kapacitet');
                else if (findresponse.result == 'neuspjesno')
                    alert('Greška! Vrijeme rezervacije more biti između 12PM i 9PM');
                else if (findresponse.result == 'datum')
                    alert('Greška! Rezervacije nisu moguće za datume prije današnjeg');
            })




    }
    restoran(event) {
        var oib = event.target.value.split('|');
        var oibb = oib[1].trim();
        this.setState({ restoran: oibb });
    }
            
            
    
    datum(event) {
        this.setState({ datum: event.target.value });
        
    }
    napraviCancel() {
        
        this.props.history.push("/klijentRezervacije");
    }
    render() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; 
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label for="kapacitet">Kapacitet:&nbsp;</Label>
                                    <Input type="text" onChange={this.kapacitet} id="kapacitet" value={this.state.kapacitet} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Datum i vrijeme:&nbsp;</Label>
                                    <input type="datetime-local" onChange={this.datum} min={today} required />

                                </FormGroup>

                               

                                <FormGroup row>
                                    <Label for="restoran">Restoran:&nbsp;</Label>
                                    <Input type="select" name="odaberi" id="restoran" onChange={this.restoran}>
                                        {this.state.restorani.map(restoran =>
                                            <option key={restoran.oib} data-key={restoran.oib}>{restoran.ime}&nbsp;|&nbsp;{restoran.oib}</option>
                                        )}
                                    </Input>
                                </FormGroup>
                                <FormGroup row>
                                    <Button color="primary" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
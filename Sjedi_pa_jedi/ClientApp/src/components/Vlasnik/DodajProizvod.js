﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';
import FileBase64 from 'react-file-base64';

export class VlasnikDodajProizvod extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id:0,
            naziv: '',
            opis: '',
            cijena: '',
            slika:null,
            idKategorije: '',
            


        };
        this.naziv = this.naziv.bind(this);
        this.opis = this.opis.bind(this);
        this.cijena = this.cijena.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }

    componentDidMount() {
        var idKategorije = this.props.match.params.id;
        this.setState({ idKategorije: idKategorije });
    }

    naziv(event) {
        this.setState({ naziv: event.target.value });

    }
    opis(event) {
        this.setState({ opis: event.target.value });
    }
    cijena(event) {
        this.setState({ cijena: event.target.value });
    }
    getFiles(files) {

        this.setState({ slika: files });
        var slika = this.state.slika.base64;
        this.setState({ slika: slika });

    }

    napraviSubmit(event) {
        var oib = this.props.match.params.oib;
        event.preventDefault();
        fetch('api/Vlasnik/PostJelo', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/vlasnikJelovnik/" + oib);
                else
                    alert('Greška! Nije moguće dodati ovo jelo');
            })
    }
        napraviCancel(){
            var oib = this.props.match.params.oib;
            this.props.history.push("/vlasnikJelovnik/" + oib);
        }


    

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
            <VlasnikNavBar oibRestorana={oib} />
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <Form>
                            <FormGroup row>
                                <Label for="naziv">Naziv</Label>
                                <Input type="text" onChange={this.naziv} id="naziv" value={this.state.naziv} required />
                            </FormGroup>

                            <FormGroup row>
                                <Label for="opis">Opis</Label>
                                <Input type="text" onChange={this.opis} id="opis" value={this.state.opis} required />
                            </FormGroup>

                            <FormGroup row>
                                <Label for="cijena">Cijena</Label>
                                <Input type="text" onChange={this.cijena} id="cijena" value={this.state.cijena} required />
                            </FormGroup>
                            <FormGroup row>
                                    <Label for="slika">Slika: &nbsp;</Label>
                                    <FileBase64 onDone={this.getFiles.bind(this)} />
                            </FormGroup>

                            <FormGroup>
                                <Button color="primary" id="submit" onClick={this.napraviSubmit}>Dodaj</Button>
                                &nbsp;
                                <Button color="danger" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
                </Container>
                </div>
        );
    }
}
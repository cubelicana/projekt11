﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class RestoranStatistika
    {
        public string ime { get; set; }
        public int brojNarudzbi { get; set; }
        public int brojRezervacija { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class Statistika
    {
        public List<KlijentStatistika> klijenti { get; set; }
        public List<RestoranStatistika> restorani { get; set; }
    }
}

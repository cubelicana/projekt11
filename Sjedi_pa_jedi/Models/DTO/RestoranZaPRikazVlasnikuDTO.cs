﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class RestoranZaPRikazVlasnikuDTO
    {
        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public string Fax { get; set; }
        public int Oib { get; set; }
        public string Iban { get; set; }
        public string ZiroRacun { get; set; }
    }
}

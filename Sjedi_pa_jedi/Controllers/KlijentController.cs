﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SjediPaJedi.Models;
using SjediPaJedi.Models.DTO;

namespace SjediPaJedi.Controllers
{
    [ApiController]
    public class KlijentController : Controller
    {
        Klijent klijent = new Klijent();
        

        [HttpGet]
        [Route("api/Klijent/GetPodatci")]
        public IActionResult GetPodatci()
        {
            var podaci = klijent.pregledajOsobnePodatke(HttpContext.User.Identity.Name);
            return Ok(podaci);
        }

        [HttpPost]
        [Route("api/Klijent/PodatciIzmjene")]
        public IActionResult PodatciIzmjene([FromBody]Korisnik korisnik)
        {
            var ime = HttpContext.User.Identity.Name;
            int rez = klijent.promijeniOsobnePodatke(korisnik,  ime);
            if (rez == 2)
                return Json(new { result = "vec postoji" });
            else if (rez == 0)
                return Json(new { result = "nije minjano" });
            else
                return Json(new { result = "uspjesno" });
            
        }

        [HttpDelete]
        [Route("api/Klijent/Delete")]
        public IActionResult Delete()
        {
            klijent.obrisiRacun(HttpContext.User.Identity.Name);
            return Ok();
        }

        [HttpGet]
        [Route("api/Klijent/GetRestoran/{oib}")]
        public IActionResult GetRestoran(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                var restoran = baza.Restoran.Where(r => r.Oib == oib).First();
                return Ok(restoran);
            }
        }

        [HttpPost]
        [Route("api/Klijent/PostStol/{oib}")]
        public IActionResult PostStol([FromBody]int sifra,[FromRoute]int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                DateTime danas = DateTime.Now;
                
                string ime = HttpContext.User.Identity.Name;
                if (baza.Stol.Any(s => s.Sifra == sifra && s.Oibrestorana == oib))
                {
                    

                    if (baza.Rezervacija.Any(r => r.SifraStola == sifra && (danas>r.Vrijeme.AddHours(-2) && danas<r.Vrijeme.AddHours(2))))
                    {
                               var rezervacija = baza.Rezervacija.Where(r => r.SifraStola == sifra && (danas > r.Vrijeme.AddHours(-2) && danas < r.Vrijeme.AddHours(2))).First();
                        
                               

                                DateTime manjePet = rezervacija.Vrijeme.AddMinutes(-5);
                                DateTime visePetnaset = rezervacija.Vrijeme.AddMinutes(15);
                                if (danas > manjePet && danas < visePetnaset)
                                {
                                    if (ime != rezervacija.KorisnickoIme)
                                        return Json(new { result = "kriviStol" });
                                    else
                                        return Json(new { result = "uspjesno" });
                                }
                                else  
                                {
                                       if (ime != rezervacija.KorisnickoIme)
                                          return Json(new { result = "rezerviraniStol" });
                                      else 
                                           return Json(new { result = "cekaj" });
     
                                }

                        
                    }
                    else
                        return Json(new { result = "uspjesno" });
                }
                else
                    return Json(new { result = "kriviStol" });
            }
        }

        [HttpGet]
        [Route("api/Klijent/GetRezervacije")]
        public IActionResult GetRezervacije()
        {
            List<RezervacijeKlijentaDTO> lista = new List<RezervacijeKlijentaDTO>();
            using (var baza = new SjediPaJediContext())
            {
                if(baza.Rezervacija.Any(s=>s.KorisnickoIme == HttpContext.User.Identity.Name))
                {
                    var rezervacije = baza.Rezervacija.Where(s => s.KorisnickoIme == HttpContext.User.Identity.Name).ToList();
                    foreach(var rezervacija in rezervacije)
                    {
                        RezervacijeKlijentaDTO rezervacijaDTO = new RezervacijeKlijentaDTO();
                        rezervacijaDTO.SifraStola = rezervacija.SifraStola;
                        rezervacijaDTO.Vrijeme = rezervacija.Vrijeme;
                        var stol = baza.Stol.Where(s => s.Sifra == rezervacijaDTO.SifraStola).First();
                        rezervacijaDTO.kapacitet = stol.Kapacitet;
                        var restoran = baza.Restoran.Where(r => r.Oib == stol.Oibrestorana).First();
                        rezervacijaDTO.ImeRestorana = restoran.Ime;
                        rezervacijaDTO.brojRezervacije = rezervacija.BrojRezervacije;
                        
                        lista.Add(rezervacijaDTO);
                    }
                    return Ok(lista);
                }
                return Json(new { result="nema"});
            }
        }
        [HttpPost]
        [Route("api/Klijent/PostRezervaciju")]
        public IActionResult PostRezervaciju([FromBody]RezervacijaStolaDTO rezervacija )
        {
            int rez = klijent.rezervirajStol(rezervacija, HttpContext.User.Identity.Name);
            if (rez == 1)
                return Json(new { result = "uspjesno" });
            else if (rez == 2)
                return Json(new { result = "neuspjesnoKapacitet" });
            else if (rez == 4)
                return Json(new { result = "neuspjesno" });
            else if (rez == 5)
                return Json(new { result = "datum" });
            else
                return Json(new { result = "neuspjesnoVrime" });
        }

        [HttpDelete]
        [Route("api/Klijent/DeleteRezervacija/{bR}")]
        public IActionResult DeleteRezervacija(int bR)
        {
            klijent.obrisiRezervaciju(bR);
            return Ok();
        }

        [HttpGet]
        [Route("api/Klijent/GetRestorane")]
        public IActionResult GetRestorane()
        {
            using (var baza = new SjediPaJediContext())
            {
                List<PopisImenaRestoranaDTO> lista = new List<PopisImenaRestoranaDTO>();
                var restorani = baza.Restoran.ToList();
                foreach(var restoran in restorani)
                {
                    PopisImenaRestoranaDTO popis = new PopisImenaRestoranaDTO();
                    popis.Ime = restoran.Ime;
                    popis.Oib = restoran.Oib;
                    lista.Add(popis);
                }
                return Ok(lista);
            }
        }

        [HttpGet]
        [Route("api/Klijent/GetRezervaciju/{bR}")]
        public IActionResult GetRezervaciju(int bR)
        {
            using (var baza = new SjediPaJediContext())
            {
                var rezervacija = baza.Rezervacija.Where(r => r.BrojRezervacije == bR).First();
                IzmjenaRezervacijeDTO rezervacijaDTO = new IzmjenaRezervacijeDTO();
                rezervacijaDTO.Datum = rezervacija.Vrijeme;
                var stol = baza.Stol.Where(s => s.Sifra == rezervacija.SifraStola).First();
                rezervacijaDTO.Kapacitet = stol.Kapacitet;
                var restoran = baza.Restoran.Where(r => r.Oib == stol.Oibrestorana).First();
                rezervacijaDTO.Restoran = restoran.Oib;
                List<PopisImenaRestoranaDTO> lista = new List<PopisImenaRestoranaDTO>();
                var restorani = baza.Restoran.ToList();
                foreach(var r in restorani)
                {
                    PopisImenaRestoranaDTO restoranDTO = new PopisImenaRestoranaDTO();
                    restoranDTO.Oib = r.Oib;
                    restoranDTO.Ime = r.Ime;
                    lista.Add(restoranDTO);
                }
                rezervacijaDTO.restorani = lista;
                return Ok(rezervacijaDTO);

            }
        }

        [HttpPost]
        [Route("api/Klijent/IzmjeniRezervaciju")]
        public IActionResult IzmjeniRezervaciju([FromBody] IzmjenaRezervacijePost rezervacija)
        {
            int rez = klijent.urediRezervaciju(rezervacija);
            if (rez == 1)
                return Json(new { result = "uspjesno" });
            else if (rez == 0)
                return Json(new { result = "prosljedi"});
            else if (rez == 2)
                return Json(new { result = "neuspjesnoKapacitet" });
            else if (rez == 4)
                return Json(new { result = "neuspjesno" });
            else if (rez == 5)
                return Json(new { result = "datum" });
            else
                return Json(new { result = "neuspjesnoVrime" });
        }

        [HttpPost]
        [Route("api/Klijent/PostJelo")]
        public IActionResult PostJelo([FromBody]JeloUKosaricuDTO jeloKosara)
        {
            using (var baza = new SjediPaJediContext())
            {
                if (SessionPomoc.GetObjectFromJson<List<JeloKosaraDTO>>(HttpContext.Session, "kosara") == null)
                {
                    var korisnik = baza.Korisnik.Where(k => k.KorisnickoIme == HttpContext.User.Identity.Name).First();
                    List<JeloKosaraDTO> lista = new List<JeloKosaraDTO>();
                    var jelo = baza.Jelo.Where(j => j.Id == jeloKosara.id).First();
                    JeloKosaraDTO jeloDTO = new JeloKosaraDTO();
                    jeloDTO.Id = jelo.Id;
                    jeloDTO.Kolicina = jeloKosara.kolicina;
                    jeloDTO.Naziv = jelo.Naziv;
                    jeloDTO.Opis = jelo.Opis;
                    jeloDTO.Slika = jelo.Slika;
                    jeloDTO.Cijena = jelo.Cijena * jeloDTO.Kolicina;
                    var kategorija = baza.Kategorija.Where(k => k.Id == jelo.Idkategorije).First();
                    jeloDTO.OIBRestorana = kategorija.Oibrestorana;
                    jeloDTO.brojKartice = korisnik.BrojKartice;
                    lista.Add(jeloDTO);


                    SessionPomoc.SetObjectAsJson(HttpContext.Session, "kosara", lista);

                }
                else
                {
                    var jelo = baza.Jelo.Where(j => j.Id == jeloKosara.id).First();
                    List<JeloKosaraDTO> lista = SessionPomoc.GetObjectFromJson<List<JeloKosaraDTO>>(HttpContext.Session, "kosara");
                    bool vecPostoji = lista.Exists(j => j.Id == jeloKosara.id);
                    if (vecPostoji == true)
                    {
                        int index = lista.FindIndex(j => j.Id == jeloKosara.id);
                        lista[index].Kolicina += jeloKosara.kolicina;
                        lista[index].Cijena = lista[index].Kolicina * jelo.Cijena;
                        SessionPomoc.SetObjectAsJson(HttpContext.Session, "kosara", lista);
                        return Json(new { result = "jeloPostoji" });
                    }
                    else
                    {

                        var korisnik = baza.Korisnik.Where(k => k.KorisnickoIme == HttpContext.User.Identity.Name).First();
                        JeloKosaraDTO jeloDTO = new JeloKosaraDTO();
                        jeloDTO.Id = jelo.Id;
                        jeloDTO.Kolicina = jeloKosara.kolicina;
                        jeloDTO.Naziv = jelo.Naziv;
                        jeloDTO.Opis = jelo.Opis;
                        jeloDTO.Slika = jelo.Slika;
                        jeloDTO.Cijena = jelo.Cijena * jeloDTO.Kolicina;
                        jeloDTO.brojKartice = korisnik.BrojKartice;
                        var kategorija = baza.Kategorija.Where(k => k.Id == jelo.Idkategorije).First();
                        jeloDTO.OIBRestorana = kategorija.Oibrestorana;


                        lista.Add(jeloDTO);
                        SessionPomoc.SetObjectAsJson(HttpContext.Session, "kosara", lista);
                    }



                }
                return Json(new { result = "uspjesno" });

            }
        }

        [HttpGet]
        [Route("api/Klijent/GetJela")]
        public IActionResult GetJela()
        {

            var jela = SessionPomoc.GetObjectFromJson<List<JeloKosaraDTO>>(HttpContext.Session, "kosara");
            if (jela == null)
                return Json(new { result = "nema" });
            else

            return Ok(jela);
        }

        [HttpDelete]
        [Route("api/Klijent/IzbrisiIzKosarice/{id}")]
        public IActionResult IzbrisiIzKosarice(int id)
        {

            List<JeloKosaraDTO> lista = SessionPomoc.GetObjectFromJson<List<JeloKosaraDTO>>(HttpContext.Session, "kosara");
            int index = lista.FindIndex(j => j.Id == id);
            lista.RemoveAt(index);
            SessionPomoc.SetObjectAsJson(HttpContext.Session, "kosara", lista);

            return Ok();
            
        }

        [HttpPost]
        [Route("api/Klijent/Plati")]
        public IActionResult Plati([FromBody]List<JeloKosaraDTO> jela)
        {
            using (var baza = new SjediPaJediContext())
            {
                SessionPomoc.SetObjectAsJson(HttpContext.Session, "kosara", null);
                klijent.platiNarudzbu(jela, HttpContext.User.Identity.Name);
                return Json(new { result="uspjesno"});
            }
            
            
        }

        [HttpPost]
        [Route("api/Klijent/Logout")]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Json(new { result="uspjesno"});
                                    
        }

        [HttpGet]
        [Route("api/Klijent/Cekanje")]
        public IActionResult Cekanje()
        {
            using (var baza = new SjediPaJediContext())
            {
                string kIme = HttpContext.User.Identity.Name;
                if (baza.Narudzba.Any(n => n.KorisnickoIme == kIme && n.Gotova == false))
                {
                    return Json(new { result = "nije" });
                }
                else
                    return Json(new { result = "gotovo" });

            }
        }

        [HttpPost]
        [Route("api/Klijent/OcijeniRestoran/{oib}")]
        public IActionResult OcijeniRestoran([FromBody]OsvrtKlijentDTO osvrt, [FromRoute]int oib)
        {
            klijent.ocijeniRestoran(osvrt, oib, HttpContext.User.Identity.Name);
            return Json(new { result = "uspjesno" });
            
        }
    }
}
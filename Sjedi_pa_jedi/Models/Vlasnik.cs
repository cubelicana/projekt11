﻿using Microsoft.AspNetCore.Http;
using SjediPaJedi.Models.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SjediPaJedi.Models
{   
    public class Vlasnik : Korisnik
    {
        public SjediPaJediContext baza = new SjediPaJediContext();
        public void obrisiStol(int sifraStola)
        {
            var stol = baza.Stol.SingleOrDefault(s => s.Sifra == sifraStola);
            baza.Remove(stol);
            if(baza.Rezervacija.Where(r=>r.SifraStola == sifraStola).ToList()!=null)
            {
                var rezervacije = baza.Rezervacija.Where(r => r.SifraStola == sifraStola).ToList();
                foreach(var rezervacija in rezervacije)
                {
                    baza.Remove(rezervacija);
                }
            }
            baza.SaveChanges();
        }
        public void promijeniKapacitet(Stol noviStol)
        {
            if (noviStol.Sifra == 0)
                return;
            var stoll = baza.Stol.SingleOrDefault(k => k.Sifra == noviStol.Sifra);
            if(stoll.Kapacitet!=noviStol.Kapacitet)
            stoll.Kapacitet = noviStol.Kapacitet;
            baza.SaveChanges();
        }
        public int dodajStol(Stol novistol)
        {
            if(baza.Stol.Any(s => s.Sifra == novistol.Sifra))
            {
                return 0;
            }
            baza.Stol.Add(novistol);
            baza.SaveChanges();
            return 1;

        }
        public void obrisiZaposlenika(String kime)
        {
            var zaposlenik = baza.PripadaRestoranu.SingleOrDefault(s => s.KorisnickoIme.Equals(kime));
            baza.Remove(zaposlenik);
            baza.SaveChanges();
        }
        public int dodajZaposlenika(String kIme, int OIBrestorana)
        {
            if (baza.PripadaRestoranu.Any(s => s.KorisnickoIme.Equals(kIme)))
            {
                return 0;
            }
            PripadaRestoranu novi = new PripadaRestoranu();
            novi.KorisnickoIme = kIme;
            novi.Oibrestorana = OIBrestorana;
            novi.RazinaOvlasti = 2;
            baza.PripadaRestoranu.Add(novi);

            var korisnik = baza.Korisnik.Where(k => k.KorisnickoIme == kIme).First();
            if (korisnik.RazinaOvlasti == 1)
                korisnik.RazinaOvlasti = 2;
            baza.SaveChanges();
            return 1;

        }
        public int dodajKategoriju(Kategorija novaKategorija)
        {
            if (baza.Kategorija.Any())
            {
                var kategorija = baza.Kategorija.Last();
                novaKategorija.Id = kategorija.Id + 1;
            }
            else
                novaKategorija.Id = 1;
            baza.Kategorija.Add(novaKategorija);
            baza.SaveChanges();
            return 1;
        }
        public void promijeniKategoriju(Kategorija novaKategorija)
        {
            if (novaKategorija.Id == 0)
                return;
            var kategorijaa = baza.Kategorija.SingleOrDefault(k => k.Id == novaKategorija.Id);
            if(kategorijaa.Naziv!=novaKategorija.Naziv)
            kategorijaa.Naziv = novaKategorija.Naziv;

            if(kategorijaa.Id!=novaKategorija.Id)
            kategorijaa.Id = novaKategorija.Id;
            baza.SaveChanges();
        }
        public void obrisiKategorija(int id)
        {
            var kategorija = baza.Kategorija.SingleOrDefault(k => k.Id == id);
            if(baza.Jelo.Where(j=>j.Idkategorije == id).ToList()!=null)
            {
                var jela = baza.Jelo.Where(j => j.Idkategorije == id).ToList();
                foreach(var jelo in jela)
                {
                    baza.Remove(jelo);
                }
            }
            baza.Remove(kategorija);
            baza.SaveChanges();
        }
        public int dodajJelo(Jelo novoJelo)
        {
            if (baza.Jelo.Any())
            {
                var jelo = baza.Jelo.Last();
                novoJelo.Id = jelo.Id + 1;
            }
            else
                novoJelo.Id = 1;
            
            baza.Jelo.Add(novoJelo);
            baza.SaveChanges();
            return 1;
        }
        public void promijeniJelo(Jelo novoJelo)
        {
            if (novoJelo.Id == 0)
                return;
            var jeloo = baza.Jelo.Where(j => j.Id == novoJelo.Id).First();
            if (jeloo.Opis != novoJelo.Opis)
                jeloo.Opis = novoJelo.Opis;

            if (jeloo.Naziv != novoJelo.Naziv)
                jeloo.Naziv = novoJelo.Naziv;

            if (jeloo.Cijena != novoJelo.Cijena)
                jeloo.Cijena = novoJelo.Cijena;

            if (jeloo.Slika != novoJelo.Slika)
                jeloo.Slika = novoJelo.Slika;

            baza.SaveChanges();

        }
        public void obrisiJelo(int id)
        {
            var jelo = baza.Jelo.SingleOrDefault(k => k.Id == id);
            if(baza.Narudzba.Where(n=>n.Idjela == id).ToList()!=null)
            {
                var narudzbe = baza.Narudzba.Where(n => n.Idjela == id).ToList();
                foreach(var narudzba in narudzbe)
                {
                    baza.Remove(narudzba);
                }
            }
            baza.Remove(jelo);
            baza.SaveChanges();
        }

        public int dodajRestoran(Restoran noviRestoran, string korisnickoIme)
        {
            if (baza.Restoran.Any(o => o.Oib == noviRestoran.Oib))
            {
                return 0;
            }
           
            baza.Restoran.Add(noviRestoran);
            PripadaRestoranu pripadaRestoranu = new PripadaRestoranu();
            pripadaRestoranu.KorisnickoIme = korisnickoIme;
            pripadaRestoranu.Oibrestorana = noviRestoran.Oib;
            pripadaRestoranu.RazinaOvlasti = 3;
            baza.PripadaRestoranu.Add(pripadaRestoranu);
            baza.SaveChanges();
            return 1;
        }

        public void obrisiRestoran(int oib)
        {
            var restoran = baza.Restoran.SingleOrDefault(res => res.Oib == oib);
            if(baza.Kategorija.Where(k=>k.Oibrestorana==oib).ToList()!=null)
            {
                var kategorije = baza.Kategorija.Where(k => k.Oibrestorana == oib).ToList();
                foreach(var kategorija in kategorije)
                {
                    baza.Remove(kategorija);
                }
            }

            if (baza.Osvrt.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var osvrti = baza.Osvrt.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var osvrt in osvrti)
                {
                    baza.Remove(osvrt);
                }
            }

            if (baza.PripadaRestoranu.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var pR = baza.PripadaRestoranu.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var p in pR)
                {
                    baza.Remove(p);
                }
            }
            if (baza.Stol.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var stolovi = baza.Stol.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var stol in stolovi)
                {
                    baza.Remove(stol);
                }
            }

            baza.Remove(restoran);
            baza.SaveChanges();
        }

        public void OdgovoriNaOsvrt(Osvrt osvrt)
        {
            if (osvrt.Id == 0)
                return;
            var osvrtt = baza.Osvrt.SingleOrDefault(k => k.Id == osvrt.Id);
            if(osvrtt.Odgovor!=osvrt.Odgovor)
            osvrtt.Odgovor = osvrt.Odgovor;
            
            baza.SaveChanges();
        }
        public List<RezervacijaDTO> pregledajRezervacijeRestorana(int oib)
        {
            var stolovi = baza.Stol.Where(s => s.Oibrestorana == oib).ToList();
            List<RezervacijaDTO> rezervacijeDTO = new List<RezervacijaDTO>();
            foreach(var stol in stolovi)
            {
                var rezervacije = baza.Rezervacija.Where(r => r.SifraStola == stol.Sifra).ToList();
                foreach(var rezervacija in rezervacije)
                {
                    RezervacijaDTO rezervacijaDTO = new RezervacijaDTO();
                    rezervacijaDTO.KorisnickoIme = rezervacija.KorisnickoIme;
                    rezervacijaDTO.BrojRezervacije = rezervacija.BrojRezervacije;
                    rezervacijaDTO.SifraStola = rezervacija.SifraStola;
                    rezervacijaDTO.Vrijeme = rezervacija.Vrijeme;
                    rezervacijeDTO.Add(rezervacijaDTO);

                }

            }
            return (rezervacijeDTO);
        }

        
    }
}

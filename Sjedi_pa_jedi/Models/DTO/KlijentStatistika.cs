﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class KlijentStatistika
    {
        public string KorisnickoIme { get; set; }
        public int brojNarudžbi { get; set; }
        public int brojRezervacija { get; set; }
    }
}

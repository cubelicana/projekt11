﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class RezervacijaDTO
    {
        public int BrojRezervacije { get; set; }
        public int SifraStola { get; set; }
        public DateTime Vrijeme { get; set; }
        public string KorisnickoIme { get; set; }
    }
}

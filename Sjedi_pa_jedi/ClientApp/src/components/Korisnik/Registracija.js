﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


export class Registracija extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            korisnickoIme: '',
            lozinka: '',
            ime: '',
            prezime: '',
            email: '',
            brojMobitela: '',
            brojKartice: '',
            razinaOvlasti: 1,

        };
        this.korisnickoIme = this.korisnickoIme.bind(this);
        this.lozinka = this.lozinka.bind(this);
        this.ime = this.ime.bind(this);
        this.prezime = this.prezime.bind(this);
        this.brojMobitela = this.brojMobitela.bind(this);
        this.email = this.email.bind(this);
        this.brojKartice = this.brojKartice.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.ajdeNaPrijavu = this.ajdeNaPrijavu.bind(this);

    }

    korisnickoIme(event) {
        this.setState({ korisnickoIme: event.target.value });
    }
    lozinka(event) {
        this.setState({ lozinka: event.target.value });
    }
    ime(event) {
        this.setState({ ime: event.target.value });
    }
    prezime(event) {
        this.setState({ prezime: event.target.value });
    }
    brojMobitela(event) {
        this.setState({ brojMobitela: event.target.value });
    }
    email(event) {
        this.setState({ email: event.target.value });
    }
    brojKartice(event) {
        this.setState({ brojKartice: event.target.value });
    }

    napraviSubmit(event) {
        event.preventDefault();
        fetch('api/Home/registracija', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/mapa");
                else
                    alert('Greška! Korisnicko ime vec postoji');
            })
          



    }
    ajdeNaPrijavu() {
        this.props.history.push("/");
    }
    render() {
        return (
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}> 
            <Form>
                <FormGroup row>
                    <Label for="korisnickoIme">Korisnicko ime</Label>
                    <Input type="text" onChange={this.korisnickoIme} id="korisnickoIme" value={this.state.korisnickoIme} required />
                </FormGroup>

                <FormGroup row>
                    <Label for="lozinka">Lozinka</Label>
                    <Input type="password"  onChange={this.lozinka} id="lozinka" value={this.state.lozinka} required/>
                </FormGroup>


                <FormGroup row>
                    <Label for="Ime">Ime</Label>
                    <Input type="text" onChange={this.ime} id="Ime" value={this.state.ime} required />
                </FormGroup>

                <FormGroup row>
                    <Label for="Prezime">Prezime</Label>
                    <Input type="text" onChange={this.prezime} id="Prezime" value={this.state.prezime} required />
                </FormGroup>

                <FormGroup row>
                    <Label for="brojMobitela">Broj mobitela</Label>
                    <Input type="text" onChange={this.brojMobitela} id="brojMobitela" value={this.state.brojMobitela} required />
                </FormGroup>

                <FormGroup row>
                    <Label for="email">Email</Label>
                    <Input type="text" onChange={this.email} id="email" value={this.state.email} required />
                </FormGroup>

                <FormGroup row>
                    <Label for="brojKreditneKartice">Broj kreditne kartice</Label>
                    <Input type="text" onChange={this.brojKartice} id="brojKreditneKartice" value={this.state.brojKartice} required />
                </FormGroup>


                <FormGroup>
                                <Button color="primary" id="submit" onClick={this.napraviSubmit} > Registracija</Button>
                                &nbsp;
                                <Button color="warning" id="prijava" onClick={this.ajdeNaPrijavu}>Prijava</Button>
                </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
﻿import React from 'react';
import { AdminNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';





export class IzKorisnik extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            korisnik: ""
        };

        var kIme = this.props.match.params.korisnickoIme;
        
        fetch('api/Admin/Izmijeni/' + kIme)
            .then(response => response.json())
            .then(korisnikk => {
                this.setState({ korisnik: korisnikk });
            });
        this.razOvlasti = this.razOvlasti.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }


    razOvlasti(event) {
        this.state.korisnik.razinaOvlasti = event.target.value;
        this.setState( this.state.korisnik );
    }
    handleCancel() {
        this.props.history.push("/adminKorisnici");
    }

    handleSubmit(event) {
        fetch('api/Admin/KorisnikIzmjene', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(response => {
                this.props.history.push("/adminKorisnici");
            });
    }

render() {
    return (
        <div>
            <AdminNavBar />
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}> 
            <Form>
                <FormGroup row>
                <Label>Korisnicko ime:&nbsp;</Label>
                    <Label>{this.state.korisnik.korisnickoIme}</Label>
                </FormGroup>

                <FormGroup row>
                    <Label>Ime:&nbsp;</Label>
                    <Label>{this.state.korisnik.ime}</Label>
                </FormGroup>

                <FormGroup row>
                <Label>Prezime: &nbsp;</Label>
                    <Label>{this.state.korisnik.prezime}</Label>
                </FormGroup>

                <FormGroup row>
                    <Label>Email: &nbsp;</Label>
                    <Label>{this.state.korisnik.email}</Label>
                </FormGroup>

                <FormGroup row>
                                <Label for="RazOvlasti">Razina ovlasti: </Label>
                                <Input type="text" value={this.state.korisnik.razinaOvlasti} onChange={this.razOvlasti} />
                </FormGroup>

                <FormGroup row>
                                <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                &nbsp;
                                <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                        </FormGroup>
               </Form>
                    </Col>
                </Row>
            </Container>

           
               
        </div>


    );
}
}

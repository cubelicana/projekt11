﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class IzmjenaRezervacijeDTO
    {
        public int Kapacitet { get; set; }
        public int Restoran { get; set; }
        public List<PopisImenaRestoranaDTO> restorani { get; set; }
        public DateTime Datum { get; set; }
    }
}

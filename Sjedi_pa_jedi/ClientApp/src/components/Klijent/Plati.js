﻿import React from 'react';
import { Card, CardTitle, CardImg, CardText, Button, CardBody, ListGroupItemText, ListGroupItemHeading, CardSubtitle, ListGroup, ListGroupItem, Container, Row, Col, InputGroup, InputGroupText, InputGroupAddon, Input } from 'reactstrap';
import { KlijentNavBar } from './NavBarBezRestorana';


export class Plati extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            jela: [],
            datum: '',
            cvv:''

        };


        fetch("api/Klijent/GetJela")
            .then(response => response.json())
            .then(jelaa => {
                this.setState({
                    jela: jelaa

                });
            })
        this.cvv = this.cvv.bind(this);
        this.datum = this.datum.bind(this);
        this.plati = this.plati.bind(this);
        this.povratak = this.povratak.bind(this);
        this.dobaviJela = this.dobaviJela.bind(this);
        
        
    }

   
    renderSlika(slika) {
        if (slika == null)
            return (<span />);
        else
            return (<img src={slika} width="75" height="75" />);
    }
    cvv(event) {
        this.setState({ cvv: event.target.value });
    }

    datum(event) {
        this.setState({ datum: event.target.value });
    }

    plati() {
        var oib = this.props.match.params.oib;
        fetch('api/Klijent/Plati', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.jela)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/klijentCekanje/" + oib);
                
            })
            
    }

    povratak() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/klijentRestoran/" + oib);
    }
   
    dobaviJela() {
        fetch("api/Klijent/GetJela")
            .then(response => response.json())
            .then(jelaa => {
                this.setState({
                    jela: jelaa

                });
            })
    }
   
    

  
    render() {
        var cijena = 0;
        var brojKartice = 0;
        this.state.jela.map(jelo =>
            cijena = cijena + jelo.cijena
        
        );
        this.state.jela.map(jelo =>
            
        brojKartice = jelo.brojKartice
        );


        return (

            <div>
                <KlijentNavBar />
                <Container>
                    <Row>

                        <Col>
                            <h2>Košarica</h2>
                            <ListGroup>
                               
                                {this.state.jela.map(jelo =>
                                    <ListGroupItem key={jelo.id}>
                                        <ListGroupItemHeading>{jelo.naziv}</ListGroupItemHeading>
                                        <ListGroupItemText>{this.renderSlika(jelo.slika)}&nbsp;<b>Opis:</b>&nbsp;{jelo.opis}&nbsp;<b>Cijena:</b>&nbsp;{jelo.cijena}&nbsp;<b>Količina:</b>&nbsp;{jelo.kolicina}
                                        
                                               
                                        </ListGroupItemText>
                                    </ListGroupItem>
                                )}
                            </ListGroup>
                            <Button color="warning" onClick={this.povratak}>Povratak na restoran</Button>

                            

                        </Col>
                        <Col>
                            <InputGroup>
                                <Input type="text" value={brojKartice} readOnly />
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>Broj kartice</InputGroupText>
                                </InputGroupAddon>
                               
                            </InputGroup>
                            <br />
                            <InputGroup>
                                
                                <InputGroupAddon addonType="append">
                                    <InputGroupText>Datum isteka</InputGroupText>
                                </InputGroupAddon>
                                <Input type="month" onChange={this.datum} />
                                
                            </InputGroup>
                            <br />
                            <InputGroup>
                                <Input type="text" onChange={this.cvv} value={this.state.cvv}/>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>CVV</InputGroupText>
                                </InputGroupAddon>
                                
                            </InputGroup>
                            <br />
                            <InputGroup>
                                
                                <InputGroupAddon addonType="append">
                                    <InputGroupText>Ukupni iznos(kn)</InputGroupText>
                                </InputGroupAddon>
                                <Input value={cijena} readOnly />
                            </InputGroup>
                            
                            <Button color="primary" onClick={this.plati}>Plati</Button>
                            
                        </Col>

                    </Row>
                </Container>
            </div>

        );
    }
}
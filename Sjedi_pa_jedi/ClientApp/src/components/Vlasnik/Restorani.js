﻿import React from 'react';
import { Table, Button, Container, Row, Col } from 'reactstrap';




export class VlasnikRestorani extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restorani: []
        };


        fetch('api/Vlasnik/GetRestorane')
            .then(response => response.json())
            .then(restorani => {
                this.setState({ restorani });
            });
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.odaberiRestoran = this.odaberiRestoran.bind(this);
        this.promjeni = this.promjeni.bind(this);
    }



    handleDelete(oib) {

        fetch('api/Vlasnik/DeleteRestoran/' + oib, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    restorani: this.state.restorani.filter((rec) => {
                        return (rec.oib != oib);
                    })
                })

        })

    }

    handleAdd() {
        this.props.history.push("/vlasnikNoviRestoran");
    }

    odaberiRestoran(oib) {
        this.props.history.push("/vlasnikZaposlenici/" + oib);
    }

    promjeni(oib) {
        this.props.history.push("/vlasnikIzRestoran/" + oib);
    }

    render() {
        
        return (
            <div>
                
                <Container>
                    <Row>
                        <Col md={{size:12, offset: 1 }}>

                <Table>
                    <thead>
                        <tr>
                            <th>Ime</th>
                            <th>Adresa</th>
                            <th>Telefon</th>
                            <th>Fax</th>
                            <th>OIB</th>
                            <th>Iban</th>
                            <th>Ziro racun</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.restorani.map(restoran =>
                            <tr key={restoran.oib}>
                                <td>{restoran.ime}</td>
                                <td>{restoran.adresa}</td>
                                <td>{restoran.telefon}</td>
                                <td>{restoran.fax}</td>
                                <td>{restoran.oib}</td>
                                <td>{restoran.iban}</td>
                                <td>{restoran.ziroRacun}</td>
                                <td>
                                    <Button color="warning" onClick={(kime) => { this.odaberiRestoran(restoran.oib) }}>Odaberi</Button>
                                    &nbsp;
                                    <Button color="success" onClick={(kime) => { this.promjeni(restoran.oib) }}>Izmjeni</Button>&nbsp;           
                                    <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovaj restoran?')) this.handleDelete(restoran.oib) }}>Izbriši</Button>
                                </td>
                            </tr>

                        )}
                    </tbody>
                        </Table>
                        </Col>
                    </Row>
                
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button color="primary" id="Dodaj" onClick={this.handleAdd}>Dodaj</Button>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
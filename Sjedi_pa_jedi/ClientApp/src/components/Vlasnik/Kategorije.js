﻿import React from 'react';
import { Table, Button, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';



export class Kategorije extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            kategorije: []
        };

        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/GetKategorije/' + oib)
            .then(response => response.json())
            .then(kategorije => {
                this.setState({ kategorije });
            });
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
    }







    handleDelete(id) {

        fetch('api/Vlasnik/DeleteKategorija/' + id, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    kategorije: this.state.kategorije.filter((rec) => {
                        return (rec.id != id);
                    })
                })

        })

    }

    handleAdd() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikDodajKategoriju/" + oib);
    }

    promijeni(id) {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikIzKategoriju/" + oib + "/" + id);
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 12, offset: 2 }}>

                <Table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Naziv</th>
                            

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.kategorije.map(kategorija =>
                            <tr key={kategorija.id}>
                                <td>{kategorija.id}</td>
                                <td>{kategorija.naziv}</td>
                                

                                            <td>
                                                <Button color="warning" onClick={(kime) => this.promijeni(kategorija.id)}>Promjeni</Button>
                                                &nbsp;
                                    <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovu kategoriju?')) this.handleDelete(kategorija.id) }}>Izbriši</Button>
                                </td>
                            </tr>

                        )}
                    </tbody>
                            </Table>
                        </Col>
                
                    
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Button color="primary" id="Dodaj" onClick={this.handleAdd}>Dodaj</Button>
                        </Col>
                    </Row>
                </Container>
            </div >
        );
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class IzmjenaRezervacijePost
    {
        public int bR { get; set; }
        public string Datum { get; set; }
        public int Kapacitet { get; set; }
       
        public int Restoran { get; set; }
        public List<Restoran> restorani { get; set; }
    }
}

﻿import React from 'react';

import { Button, Card, CardTitle, CardBody, CardText, CardSubtitle, Container, Row, Col } from 'reactstrap';
import { KlijentNavBar } from './NavBarBezRestorana';

export class KlijentRezervacije extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           rezervacije:[]



        };

        

        fetch('api/Klijent/GetRezervacije')
            .then(response => response.json())
            .then(rezervacije => {
                if (rezervacije.result == 'nema')
                    this.setState({ rezervacije: [] })
                else
                this.setState({ rezervacije });
            });

        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);


    }
   
    handleDelete(bR) {
        fetch('api/Klijent/DeleteRezervacija/' + bR, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    rezervacije: this.state.rezervacije.filter((rec) => {
                        return (rec.brojRezervacije != bR);
                    })
                })

        })
    }
   
    handleAdd() {
        this.props.history.push("/klijentNovaRezervacija");
    }

    handleEdit(bR) {
        this.props.history.push("/klijentIzRezervaciju/" + bR);
    }
   
    render() {
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            
                            {this.state.rezervacije.map(rezervacija =>
                                <Card key={rezervacija.brojRezervacije}>
                                <CardBody>

                                    <CardTitle>Restoran: &nbsp;{rezervacija.imeRestorana}</CardTitle>
                                    <CardSubtitle>Vrijeme:&nbsp;{rezervacija.vrijeme}</CardSubtitle>
                                        <CardText>Šifra stola:&nbsp; {rezervacija.sifraStola} &nbsp;Kapacitet:&nbsp;{rezervacija.kapacitet}</CardText>
                                        <Button color="warning" onClick={(kIme) => this.handleEdit(rezervacija.brojRezervacije)}>Izmjeni</Button>
                                        &nbsp;
                                        <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovu rezervaciju?')) this.handleDelete(rezervacija.brojRezervacije) }}>Izbriši</Button>

                                    </CardBody>
                                </Card>
                                )}
                            
                            <Button color="primary" onClick={ this.handleAdd}>Dodaj</Button>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
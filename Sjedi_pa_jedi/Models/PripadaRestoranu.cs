﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class PripadaRestoranu
    {
        public string KorisnickoIme { get; set; }
        public int Oibrestorana { get; set; }
        public int RazinaOvlasti { get; set; }
    }
}

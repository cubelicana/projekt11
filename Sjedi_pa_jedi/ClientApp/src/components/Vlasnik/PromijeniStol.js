﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';




export class IzStol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stol: ""
        };

        var sifra = this.props.match.params.sifra;

        fetch('api/Vlasnik/GetStol/' + sifra)
            .then(response => response.json())
            .then(stoll => {
                this.setState({ stol: stoll });
            });
        this.kapacitet = this.kapacitet.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);

    }


    kapacitet(event) {
        this.state.stol.kapacitet = event.target.value;
        this.setState(this.state.stol);
    }

    handleCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikStolovi/" + oib);
    }

    handleSubmit(event) {
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/StolKapacitet/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            this.props.history.push("/vlasnikStolovi/"+oib);
        });
    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>
                                

                                <FormGroup row>
                                    <Label>Sifra: &nbsp;</Label>
                                    <Label>{this.state.stol.sifra}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="Kapacitet">Kapacitet: </Label>
                                    <Input type="text" value={this.state.stol.kapacitet} onChange={this.kapacitet} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>



            </div>


        );
    }
}
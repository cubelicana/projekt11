﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';


export class KlijentOsvrti extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            osvrti: []

        };

        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/GetOsvrte/' + oib)
            .then(response => response.json())
            .then(osvrti => {
                this.setState({ osvrti });
            });


        this.vrati = this.vrati.bind(this);
    }

    vrati() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/klijentStol/" + oib);
    }

    render() {
        
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            {this.state.osvrti.map(osvrt =>
                                <Card>
                                    <CardBody>

                                        <CardTitle>Korisnik: &nbsp;{osvrt.korisnickoIme}</CardTitle>
                                        <CardSubtitle>Ocjena:&nbsp;{osvrt.ocjena}</CardSubtitle>
                                        <CardText>Opis:&nbsp; {osvrt.opis}</CardText>
                                        <CardText>Odgovor:&nbsp; {osvrt.odgovor}</CardText>

                                        

                                    </CardBody>
                                </Card>
                                
                            )}
                            <Button color="primary" onClick={ this.vrati } >Povratak na restoran</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
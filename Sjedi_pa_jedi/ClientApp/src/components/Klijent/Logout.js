﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';


export class Logout extends React.Component {
    constructor(props) {
        super(props);
       
        fetch('api/Klijent/Logout', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify()
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/");
            })
    }

   

    render() {

        return (<br />);
            
    }
}
﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Narudzba
    {
        public int Id { get; set; }
        public int Idjela { get; set; }
        public TimeSpan Vrijeme { get; set; }
        public string KorisnickoIme { get; set; }
        public bool Gotova { get; set; }

        public virtual Jelo IdjelaNavigation { get; set; }
        public virtual Korisnik KorisnickoImeNavigation { get; set; }
    }
}

﻿import React from 'react';
import { VlasnikNavBar } from './NavBar';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

import FileBase64 from 'react-file-base64';
export class IzProizvod extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            jelo: ""
        };

        var id = this.props.match.params.id;

        fetch('api/Vlasnik/GetJelo/' + id)
            .then(response => response.json())
            .then(jeloo => {
                this.setState({ jelo: jeloo });
            });
        this.naziv = this.naziv.bind(this);
        this.opis = this.opis.bind(this);
        this.cijena = this.cijena.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);

    }


    naziv(event) {
        this.state.jelo.naziv = event.target.value;
        this.setState(this.state.jelo);
    }
    opis(event) {
        this.state.jelo.opis = event.target.value;
        this.setState(this.state.jelo);
    }
    cijena(event) {
        this.state.jelo.cijena = event.target.value;
        this.setState(this.state.jelo);
    }

    handleCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikJelovnik/" + oib);
    }

    handleSubmit(event) {
        var oib = this.props.match.params.oib;
        fetch('api/Vlasnik/PromjeniJelo/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            this.props.history.push("/vlasnikJelovnik/" + oib);
        });
    }

    getFiles(files) {
        var slikaa = files;
        
        var slika = slikaa.base64;
        this.state.jelo.slika = slika;
        this.setState(this.state.jelo);

    }

    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <VlasnikNavBar oibRestorana={oib} />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>


                                <FormGroup row>
                                    <Label>Naziv: </Label>
                                    <Input type="text" value={this.state.jelo.naziv} onChange={this.naziv} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Opis: </Label>
                                    <Input type="text" value={this.state.jelo.opis} onChange={this.opis} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label>Cijena: </Label>
                                    <Input type="text" value={this.state.jelo.cijena} onChange={this.cijena} />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="slika">Slika: &nbsp;</Label>
                                    <img src={this.state.jelo.slika} width="75" height="75" />&nbsp;
                                    <FileBase64 onDone={this.getFiles.bind(this)} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="success" id="submit" onClick={this.handleSubmit}>Spremi promjene</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={this.handleCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>



            </div>


        );
    }
}
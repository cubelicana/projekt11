﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class PopisImenaRestoranaDTO
    {
       
        public string Ime { get; set; }
        public int Oib { get; set; }
    }
}

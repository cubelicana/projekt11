﻿import React from 'react';
import { AdminNavBar } from './NavBar';
import { Table, Button } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';



export class Korisnici extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            korisnici: [],
            
        };
        
        
        fetch('api/Admin/GetKorisnici')
                .then(response => response.json())
            .then(korisnici => {
                
                
                    this.setState({
                        korisnici
                        
                    });
            });
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    



    
   
    handleDelete(kIme) {
       
        fetch('api/Admin/Delete/' + kIme, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    korisnici: this.state.korisnici.filter((rec) => {
                        return (rec.korisnickoIme != kIme);
                    })
                })

        })
        
    }

    handleEdit(kIme) {
        this.props.history.push("/adminKorisnik/" + kIme);
    }
   

    render() {
        return (
            <div>
                <AdminNavBar />
                <Container>
                    <Row>
                        <Col>
            <Table>
                <thead>
                    <tr>
                        <th>Korisnicko ime</th>
                        <th>Ime</th>
                        <th>Prezime</th>
                        <th>Email</th>
                        <th>Razina ovlasti</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.korisnici.map(korisnik =>
                        <tr key={korisnik.korisnickoIme}>
                            <td>{korisnik.korisnickoIme}</td>
                            <td>{korisnik.ime}</td>
                            <td>{korisnik.prezime}</td>
                            <td>{korisnik.email}</td>
                            <td>{korisnik.razinaOvlasti}</td>
                            <td>
                                    <Button color="primary" onClick={(kIme) => this.handleEdit(korisnik.korisnickoIme)}>Promjeni</Button>
                                    &nbsp;
                                <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovog korisnika?')) this.handleDelete(korisnik.korisnickoIme) } }>Izbriši</Button>
                            </td>
                        </tr>

                    )}
                </tbody>
                            </Table>
                            </Col>
                    </Row>
                </Container>
              </div >
        );
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SjediPaJedi.Models
{
    public partial class Korisnik
    {
        public Korisnik()
        {
            Narudzba = new HashSet<Narudzba>();
            Osvrt = new HashSet<Osvrt>();
            Rezervacija = new HashSet<Rezervacija>();
        }

        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string BrojMobitela { get; set; }
        public string Email { get; set; }
        public int BrojKartice { get; set; }
        public int RazinaOvlasti { get; set; }

        public virtual ICollection<Narudzba> Narudzba { get; set; }
        public virtual ICollection<Osvrt> Osvrt { get; set; }
        public virtual ICollection<Rezervacija> Rezervacija { get; set; }

        public SjediPaJediContext baza = new SjediPaJediContext();
        public int Registracija()
        {
            using (var baza = new SjediPaJediContext())
            {
                if (baza.Korisnik.Any(o => o.KorisnickoIme == this.KorisnickoIme))
                {
                    return 0;
                }
                baza.Korisnik.Add(this);
                baza.SaveChanges();
                return 1;
            }
        }
    }
}


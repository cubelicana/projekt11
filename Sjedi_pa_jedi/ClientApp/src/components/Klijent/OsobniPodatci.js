﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Card, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, FormGroup, Form, Label,Button } from 'reactstrap';


export class OsobniPodatci extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            podaci:""

        };

        
        fetch('api/Klijent/GetPodatci')
            .then(response => response.json())
            .then(podaci => {
                this.setState({ podaci });
            });

        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

    }

    handleDelete() {
        fetch('api/Klijent/Delete',{
            method: 'delete'
        }).then(data => {
            this.props.history.push("/");

        })
    }

    
    napraviSubmit() {
        this.props.history.push("/izmjeniPodatke");
    }



    render() {
       
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 8, offset: 4 }}>
                            
                            <Form>

                                <FormGroup row>
                                    <Label><b>Korisničko ime:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.korisnickoIme}</Label>
                                </FormGroup>
    
    
                                <FormGroup row>

                                    <Label><b>Lozinka:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.lozinka}</Label>

                                </FormGroup>

                                <FormGroup row>
                                    <Label for="telefon"><b>Ime:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.ime}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="fax"><b>Prezime:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.prezime}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="iban"><b>Broj mobitela:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.brojMobitela}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="ziroRacun"><b>Email:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.email}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="oib"><b>Broj kartice:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.brojKartice}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="slika"><b>Razina ovlasti:&nbsp;</b></Label>
                                    <Label>{this.state.podaci.razinaOvlasti}</Label>
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="warning" onClick={this.napraviSubmit}>Promjeni</Button>
                                    &nbsp;
                                    <Button color="danger" id="cancel" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati svoj račun?')) this.handleDelete() }}>Izbriši račun</Button>
                                </FormGroup>
                            </Form>
                            
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
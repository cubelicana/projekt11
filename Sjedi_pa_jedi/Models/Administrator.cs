﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SjediPaJedi.Models;
using SjediPaJedi.Models.DTO;

namespace SjediPaJedi.Models
{
    public class Administrator : Korisnik
    {
        public SjediPaJediContext baza = new SjediPaJediContext();
      public void obrisiKorisnike(String kIme)
        {
            var korisnik = baza.Korisnik.SingleOrDefault(korisnikk => korisnikk.KorisnickoIme == kIme);
            if (baza.PripadaRestoranu.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var pR = baza.PripadaRestoranu.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var p in pR)
                {
                    baza.Remove(p);
                }
            }
            if (baza.Osvrt.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var osvrti = baza.Osvrt.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var osvrt in osvrti)
                {
                    baza.Remove(osvrt);
                }
            }
            if (baza.Narudzba.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var narudzbe = baza.Narudzba.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var narudzba in narudzbe)
                {
                    baza.Remove(narudzba);
                }
            }
            if (baza.Rezervacija.Where(k => k.KorisnickoIme == kIme).ToList() != null)
            {
                var rezervacije = baza.Rezervacija.Where(k => k.KorisnickoIme == kIme).ToList();
                foreach (var rezervacija in rezervacije)
                {
                    baza.Remove(rezervacija);
                }
            }
            baza.Remove(korisnik);
            baza.SaveChanges();
            
        }

        public void promijeniPravaPristupa(KorisnikDTO korisnik)
        {
            if (korisnik.KorisnickoIme == null)
                return;
            var korisnikk = baza.Korisnik.Where(k => k.KorisnickoIme == korisnik.KorisnickoIme).First();
            if(korisnikk.RazinaOvlasti!=korisnik.RazinaOvlasti)
            korisnikk.RazinaOvlasti = korisnik.RazinaOvlasti;
            baza.SaveChanges();
        }

        public void obrisiRecenzije(int id)
        {
            var osvrt = baza.Osvrt.SingleOrDefault(res => res.Id == id);
            baza.Remove(osvrt);
            baza.SaveChanges();
        }

        public int dodajRestoran(RestoranSVlasnikomDTO noviRestoran)
        {
            if (baza.Restoran.Any(o => o.Oib == noviRestoran.Oib))
            {
                return 0;
            }
            Restoran restoran = new Restoran();
            restoran.Adresa = noviRestoran.Adresa;
            restoran.Fax = noviRestoran.Fax;
            restoran.Iban = noviRestoran.Iban;
            restoran.Ime = noviRestoran.Ime;
            restoran.Oib = noviRestoran.Oib;
           
            restoran.Slika = noviRestoran.Slika;
            restoran.Telefon = noviRestoran.Telefon;
            restoran.ZiroRacun = noviRestoran.ZiroRacun;
            baza.Restoran.Add(restoran);
            PripadaRestoranu pripadaRestoranu = new PripadaRestoranu();
            pripadaRestoranu.KorisnickoIme = noviRestoran.ImeVlasnika;
            pripadaRestoranu.Oibrestorana = noviRestoran.Oib;
            pripadaRestoranu.RazinaOvlasti = 3;
            baza.PripadaRestoranu.Add(pripadaRestoranu);
            baza.SaveChanges();
            return 1;
        }

        public void obrisiRestoran(int oib)
        {
            var restoran = baza.Restoran.SingleOrDefault(res => res.Oib == oib);
            if (baza.Kategorija.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var kategorije = baza.Kategorija.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var kategorija in kategorije)
                {
                    baza.Remove(kategorija);
                }
            }

            if (baza.Osvrt.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var osvrti = baza.Osvrt.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var osvrt in osvrti)
                {
                    baza.Remove(osvrt);
                }
            }

            if (baza.PripadaRestoranu.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var pR = baza.PripadaRestoranu.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var p in pR)
                {
                    baza.Remove(p);
                }
            }
            if (baza.Stol.Where(k => k.Oibrestorana == oib).ToList() != null)
            {
                var stolovi = baza.Stol.Where(k => k.Oibrestorana == oib).ToList();
                foreach (var stol in stolovi)
                {
                    baza.Remove(stol);
                }
            }
            baza.Remove(restoran);
            
            baza.SaveChanges();
        }
    }
}

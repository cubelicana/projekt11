import React, { Component } from 'react';
import { Route } from 'react-router';
import { MapContainer } from './components/Klijent/Mapa';
import { Registracija } from './components/Korisnik/Registracija';
import { Prijava } from './components/Klijent/Prijava';
import { Korisnici } from './components/Admin/Korisnici';
import { IzKorisnik } from './components/Admin/IzmijeniKorisnik';
import { AdminRestorani } from './components/Admin/Restorani';
import { AdminDodajRestoran } from './components/Admin/DodajRestoran';
import { AdminOsvrti } from './components/Admin/Osvrti';
import { Restoran } from './components/Korisnik/Restoran';
import { VlasnikDodajRestoran } from './components/Vlasnik/DodajRestoran';
import { VlasnikRestorani } from './components/Vlasnik/Restorani';
import { Zaposlenici } from './components/Vlasnik/Zaposlenici';
import { VlasnikDodajZaposlenika } from './components/Vlasnik/DodajZaposlenika';
import { VlasnikStolovi } from './components/Vlasnik/Stolovi';
import { VlasnikDodajStol } from './components/Vlasnik/DodajStol';
import { IzStol } from './components/Vlasnik/PromijeniStol';
import { Kategorije } from './components/Vlasnik/Kategorije';
import { VlasnikDodajKategoriju } from './components/Vlasnik/DodajKategoriju';
import { IzKategoriju } from './components/Vlasnik/PromijeniKategoriju';
import { VlasnikOsvrti } from './components/Vlasnik/Osvrti';
import { OdgovoriNaOsvrt } from './components/Vlasnik/OdgovoriNaOsvrt';
import { VlasnikJelovnik } from './components/Vlasnik/Jelovnik';
import { VlasnikDodajProizvod } from './components/Vlasnik/DodajProizvod';
import { IzProizvod } from './components/Vlasnik/PromjeniProizvod';
import { VlasnikRezervacije } from './components/Vlasnik/Rezervacije';
import { OsobniPodatci } from './components/Klijent/OsobniPodatci';
import { IzPodatke } from './components/Klijent/PromjeniPodatke';
import { PogledajAktivneNarudzbe } from './components/Zaposlenik/PogledajAktivneNarudzbe';
import { KlijentStol } from './components/Klijent/Stol';
import { KlijentRestoran } from './components/Klijent/Restoran';
import { KlijentOsvrti } from './components/Klijent/Osvrti';
import { KlijentRezervacije } from './components/Klijent/Rezervacije';
import { RezervirajStol } from './components/Klijent/RezervirajStol';
import { IzRezervaciju } from './components/Klijent/IzmjeniRezervaciju';
import { KlijentOsvrti2 } from './components/Klijent/KlijentOsvrti';
import { Plati } from './components/Klijent/Plati';
import { Logout } from './components/Klijent/Logout';
import { CekanjeNarudzbe } from './components/Klijent/CekanjeNarudzbe';
import { NapisiOsvrt } from './components/Klijent/NapisiOsvrt';
import { IzRestoran } from './components/Vlasnik/PromjeniRestoran';
import { Statistika } from './components/Admin/Statistika';



export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <div>
               <Route exact path='/' component={Prijava} />
               <Route path="/mapa" component={MapContainer} />
                <Route path="/registracija" component={Registracija} />
                <Route path="/adminStatistika" component={Statistika} />
               <Route path="/adminKorisnici" component={Korisnici} />
                <Route path="/adminKorisnik/:korisnickoIme" component={IzKorisnik} />
                <Route path="/adminRestorani" component={AdminRestorani} />
                <Route path="/adminRestoran" component={AdminDodajRestoran} />
                <Route path="/adminOsvrti" component={AdminOsvrti} />
                <Route path="/restoran/:oib" component={Restoran} />
                <Route path="/vlasnikRestorani" component={VlasnikRestorani} />
                <Route path="/vlasnikNoviRestoran" component={VlasnikDodajRestoran} />
                <Route path="/vlasnikZaposlenici/:oib" component={Zaposlenici} />
                <Route path="/vlasnikDodajZaposlenika/:oib" component={VlasnikDodajZaposlenika} />
                <Route path="/vlasnikStolovi/:oib" component={VlasnikStolovi} />
                <Route path="/vlasnikNoviStol/:oib" component={VlasnikDodajStol} />
                <Route path="/vlasnikIzStol/:oib/:sifra" component={IzStol} />
                <Route path="/vlasnikKategorije/:oib" component={Kategorije} />
                <Route path="/vlasnikDodajKategoriju/:oib" component={VlasnikDodajKategoriju} />
                <Route path="/vlasnikIzKategoriju/:oib/:id" component={IzKategoriju} />
                <Route path="/vlasnikOsvrti/:oib" component={VlasnikOsvrti} />
                <Route path="/vlasnikOdgovorNaOsvrt/:oib/:id" component={OdgovoriNaOsvrt} />
                <Route path="/vlasnikJelovnik/:oib" component={VlasnikJelovnik} />
                <Route path="/vlasnikDodajProizvod/:oib/:id" component={VlasnikDodajProizvod} />
                <Route path="/vlasnikIzProizvod/:oib/:id" component={IzProizvod} />
                <Route path="/vlasnikRezervacije/:oib" component={VlasnikRezervacije} />
                <Route path="/vlasnikIzRestoran/:oib" component={IzRestoran} />
                <Route path="/zaposlenikAktivneNarudzbe" component={PogledajAktivneNarudzbe} />
                <Route path="/osobniPodatci" component={OsobniPodatci} />
                <Route path="/izmjeniPodatke" component={IzPodatke} />
                <Route path="/klijentStol/:oib" component={KlijentStol} />
                <Route path="/klijentRestoran/:oib" component={KlijentRestoran} />
                <Route path="/klijentOsvrti/:oib" component={KlijentOsvrti} />
                <Route path="/klijentRezervacije" component={KlijentRezervacije} />
                <Route path="/klijentNovaRezervacija" component={RezervirajStol} />
                <Route path="/klijentIzRezervaciju/:bR" component={IzRezervaciju} />
                <Route path="/klijentRecenzije/:oib" component={KlijentOsvrti2} />
                <Route path="/klijentNarudzba/:oib" component={Plati} />
                <Route path="/klijentLogout" component={Logout} />
                <Route path="/klijentCekanje/:oib" component={CekanjeNarudzbe} />
                <Route path="/klijentNapisiOsvrt/:oib" component={NapisiOsvrt} />
                
                                
                              
            </div>

        );
    }
}
﻿import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export class VlasnikNavBar extends Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true,
            ruta1: '',
            ruta2: '',
            ruta3: '',
            ruta4: '',
            ruta5: ''
            
        };

        
    }

    
   
    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    componentDidMount() {
        var oib = this.props.oibRestorana;
        this.setState({
            ruta1: "/vlasnikZaposlenici/" + oib,
            ruta2: "/vlasnikStolovi/" + oib,
            ruta3: "/vlasnikRezervacije/" + oib,
            ruta4: "/vlasnikOsvrti/" + oib,
            ruta5: "/vlasnikJelovnik/" + oib,
            ruta6: "/vlasnikKategorije/" +oib

        });
    }
    render() {
        return (
            <div>
                <Navbar color="faded" light>
                    <NavbarBrand href="/vlasnikRestorani" className="mr-auto">SjediPaJedi</NavbarBrand>
                    <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                    <Collapse isOpen={!this.state.collapsed} navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink href="/vlasnikRestorani">Restorani</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta1}> Zaposlenici</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta2}>Stolovi</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta3}>Rezervacije</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta4}>Osvrti</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta5}>Jelovnik</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href={this.state.ruta6}>Kategorije</NavLink>
                            </NavItem>

                            <NavItem>
                                <NavLink href="/klijentLogout">Odjava</NavLink>
                            </NavItem>
                           
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}
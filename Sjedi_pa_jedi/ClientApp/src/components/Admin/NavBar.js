﻿import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand,Nav, NavItem, NavLink } from 'reactstrap';

export class AdminNavBar extends Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    render() {
        return (
            <div>
                <Navbar color="faded" light>
                    <NavbarBrand href="/adminKorisnici" className="mr-auto">SjediPaJedi</NavbarBrand>
                    <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                    <Collapse isOpen={!this.state.collapsed} navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink href="/adminKorisnici">Korisnici</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/adminRestorani">Restorani</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/adminOsvrti">Osvrti</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/adminStatistika">Statistika</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/klijentLogout">Odjava</NavLink>
                            </NavItem>

                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}
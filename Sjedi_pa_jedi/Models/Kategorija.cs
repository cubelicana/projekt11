﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Kategorija
    {
        public Kategorija()
        {
            Jelo = new HashSet<Jelo>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }
        public int Oibrestorana { get; set; }

        public virtual Restoran OibrestoranaNavigation { get; set; }
        public virtual ICollection<Jelo> Jelo { get; set; }
    }
}

﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


export class NapisiOsvrt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ocjena: '5',
            komentar: ''

        };

        

        this.ocjena = this.ocjena.bind(this);
        this.komentar = this.komentar.bind(this);
        

        this.napraviSubmit = this.napraviSubmit.bind(this);      
        this.napraviCancel = this.napraviCancel.bind(this);


    }


    ocjena(event) {
        this.setState({ ocjena: event.target.value });
    }
    komentar(event) {
        this.setState({ komentar: event.target.value });
    }
    

    napraviSubmit(event) {
        event.preventDefault();
        var oib = this.props.match.params.oib;
        fetch('api/Klijent/OcijeniRestoran/'+oib, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/mapa");
                
            })




    }
    napraviCancel() {

        this.props.history.push("/mapa");
    }
    render() {
        return (
            <div>
                
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>

                                <FormGroup row>
                                    <Label for="ocjena">Ocjena:&nbsp;</Label>
                                    <Input type="select" id="ocjena" onChange={this.ocjena} value={this.state.ocjena} required >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </Input>
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="komentar">Komentar:&nbsp;</Label>
                                    <Input type="text" id="komentar" onChange={this.komentar} />
                                </FormGroup>


                                

                                <FormGroup row>
                                    <Button color="primary" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
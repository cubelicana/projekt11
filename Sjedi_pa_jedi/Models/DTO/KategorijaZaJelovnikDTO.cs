﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class KategorijaZaJelovnikDTO
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public virtual List<JeloVlasnikDTO> Jelo { get; set; }
    }
}

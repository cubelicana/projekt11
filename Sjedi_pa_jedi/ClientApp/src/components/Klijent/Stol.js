﻿import React from 'react';
import { KlijentNavBar } from './NavBarBezRestorana';
import { Card, CardImg, Container, Row, Col, CardText, CardBody, CardTitle, CardSubtitle, Button , Label, Input} from 'reactstrap';


export class KlijentStol extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restoran: "",
            stol:""

        };

        var oib = this.props.match.params.oib;
        fetch('api/Klijent/GetRestoran/' + oib)
            .then(response => response.json())
            .then(restoran => {
                this.setState({ restoran });
            });


        this.odaberi = this.odaberi.bind(this);
        this.stol = this.stol.bind(this);
    }



    stol(event) {
        this.setState({ stol: event.target.value });
    }


    odaberi(sifra) {
        var oib = this.props.match.params.oib;
        fetch('api/Klijent/PostStol/'+oib, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.stol)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/klijentRestoran/" + oib);
                else if (findresponse.result == 'kriviStol')
                    alert('Greška! Unijeli ste krivu sifru stola');
                else if (findresponse.result == 'rezerviranStol')
                    alert('Greška! Unijeli ste sifru stola koji je rezerviran');
                else
                    alert('Greška! Pokušajte ponovno');
            })
    }

    pogledajOsvrte(oib) {
        this.props.history.push("/klijentOsvrti/" + oib);
    }
    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            
                                <Card>
                                    <CardBody>
                                    <CardImg src={this.state.restoran.slika} width="100%" />
                                        <CardTitle>Ime: &nbsp;{this.state.restoran.ime}</CardTitle>
                                        <CardSubtitle>Adresa:&nbsp;{this.state.restoran.adresa}</CardSubtitle>
                                    <CardText>OIB:&nbsp; {this.state.restoran.oib}</CardText>
                                    <CardText>Telefon:&nbsp; {this.state.restoran.telefon}</CardText>
                                    <CardText>Fax:&nbsp; {this.state.restoran.fax}</CardText>
                                    <CardText>Iban:&nbsp; {this.state.restoran.iban}</CardText>
                                    <CardText>Žiro račun:&nbsp; {this.state.restoran.ziroRacun}</CardText>
                                    <Label for="naziv">Unesi šifru stola:&nbsp;</Label>
                                    <Input type="text" onChange={this.stol} id="naziv" value={this.state.stol} required />
                                    
                                    <Button color="primary" onClick={(kIme) => this.odaberi(this.state.stol)}>Unesi</Button>
                                    &nbsp;
                                    <Button color="success" onClick={(kIme) => this.pogledajOsvrte(this.state.restoran.oib)}>Pogledaj osvrte</Button>

                                    



                                    </CardBody>
                                </Card>
                          
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
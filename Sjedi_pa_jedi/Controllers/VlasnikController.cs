﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SjediPaJedi.Models;
using SjediPaJedi.Models.DTO;

namespace SjediPaJedi.Controllers
{

    [ApiController]
    public class VlasnikController : Controller
    {
        Vlasnik vlasnik = new Vlasnik();
       
        [HttpGet]
        [Route("api/Vlasnik/GetRestorane")]
        public IActionResult GetRestorane()
        {
            using (var baza = new SjediPaJediContext())
            {
                List<RestoranZaPRikazVlasnikuDTO> restorani = new List<RestoranZaPRikazVlasnikuDTO>();
                var pripadaRestoranu = baza.PripadaRestoranu.Where(r => r.KorisnickoIme == HttpContext.User.Identity.Name).ToList();
                foreach (var pR in pripadaRestoranu)
                {
                    var restoran = baza.Restoran.Where(r => r.Oib == pR.Oibrestorana).First();
                    RestoranZaPRikazVlasnikuDTO pom = new RestoranZaPRikazVlasnikuDTO();
                    pom.Adresa = restoran.Adresa;
                    pom.Fax = restoran.Fax;
                    pom.Iban = restoran.Iban;
                    pom.Ime = restoran.Ime;
                    pom.Oib = restoran.Oib;
                    pom.Telefon = restoran.Telefon;
                    pom.ZiroRacun = restoran.ZiroRacun;
                    
                    restorani.Add(pom);
                }
                return Ok(restorani);
            }
        }
        [HttpDelete]
        [Route("api/Vlasnik/DeleteRestoran/{oib}")]
        public IActionResult DeleteRestoran(int oib)
        {
            vlasnik.obrisiRestoran(oib);
            return Ok();

        }
        [HttpPost]
        [Route("api/Vlasnik/PostRestoran")]
        public IActionResult PostRestoran([FromBody]Restoran noviRestoran)
        {
            int rez = vlasnik.dodajRestoran(noviRestoran, HttpContext.User.Identity.Name);
            if (rez == 0)
                return Json(new { result = "neuspjesno" });
            else
                return Json(new { result = "uspjesno" });
        }

        [HttpPost]
        [Route("api/Vlasnik/PromjeniRestoran")]
        public IActionResult PromjeniRestoran([FromBody]Restoran restoran)
        {
            using (var baza = new SjediPaJediContext())
            {
                if (restoran.Ime == null)
                    return Json(new { result = "nije minjano" });

                var Restoran = baza.Restoran.Where(r => r.Oib == restoran.Oib).First();
                if (Restoran.Ime != restoran.Ime)
                    Restoran.Ime = restoran.Ime;

                if (Restoran.Adresa != restoran.Adresa)
                    Restoran.Adresa = restoran.Adresa;

                if (Restoran.Telefon != restoran.Telefon)
                    Restoran.Telefon = restoran.Telefon;

                if (Restoran.Fax != restoran.Fax)
                    Restoran.Fax = restoran.Fax;

                if (Restoran.Iban != restoran.Iban)
                    Restoran.Iban = restoran.Iban;

                if (Restoran.ZiroRacun != restoran.ZiroRacun)
                    Restoran.ZiroRacun = restoran.ZiroRacun;

                if (Restoran.Slika != restoran.Slika)
                    Restoran.Slika = restoran.Slika;
                baza.SaveChanges();
                return Json(new { result = "uspjesno" });

            }
        }

        [HttpGet]
        [Route("api/Vlasnik/GetRestoran/{oib}")]
        public IActionResult GetRestoran(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                var Restoran = baza.Restoran.Where(r => r.Oib == oib).First();
                RestoranDTO restoran = new RestoranDTO();
                restoran.Oib = Restoran.Oib;
                restoran.Adresa = Restoran.Adresa;
                restoran.Fax = Restoran.Fax;
                restoran.Iban = Restoran.Iban;
                restoran.Ime = Restoran.Ime;
                restoran.Slika = Restoran.Slika;
                restoran.ZiroRacun = Restoran.ZiroRacun;
                restoran.Telefon = Restoran.Telefon;
                
                return Ok(restoran);
            }
        }

        [HttpDelete]
        [Route("api/Vlasnik/DeleteZaposlenik/{kime}")]
        public IActionResult DeleteZaposlenik(String kime)
        {
            vlasnik.obrisiZaposlenika(kime);
            return Ok();

        }

        [HttpPost]
        [Route("api/Vlasnik/PostZaposlenik")]
        public IActionResult PostZaposlenik([FromBody]PripadaRestoranu pR)
        {
            using (var baza = new SjediPaJediContext())
            {

                int rez = vlasnik.dodajZaposlenika(pR.KorisnickoIme,pR.Oibrestorana);
                if (rez == 0)
                    return Json(new { result = "neuspjesno" });
                else
                    return Json(new { result = "uspjesno" });
            }
        }

        [HttpGet]
        [Route("api/Vlasnik/GetZaposlenike/{oib}")]
        public IActionResult GetZaposlenike(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                List<KorisnikDTO> zaposlenici = new List<KorisnikDTO>();


                var pripadaRestoranu = baza.PripadaRestoranu.Where(r => r.Oibrestorana == oib && r.RazinaOvlasti == 2).ToList();
                foreach(var pR in pripadaRestoranu)
                {
                    var korisnik = baza.Korisnik.Where(r => r.KorisnickoIme == pR.KorisnickoIme).First();
                    KorisnikDTO korisnikDTO = new KorisnikDTO();
                    korisnikDTO.Ime = korisnik.Ime;
                    korisnikDTO.KorisnickoIme = korisnik.KorisnickoIme;
                    korisnikDTO.Prezime = korisnik.Prezime;
                    korisnikDTO.RazinaOvlasti = korisnik.RazinaOvlasti;
                    korisnikDTO.Email = korisnik.Email;
                    zaposlenici.Add(korisnikDTO);
                }
                return Ok(zaposlenici);
            }
            
        }

        [HttpDelete]
        [Route("api/Vlasnik/DeleteStol/{sifra}")]
        public IActionResult DeleteStol(int sifra)
        {
            vlasnik.obrisiStol(sifra);
            return Ok();

        }
        [HttpPost]
        [Route("api/Vlasnik/PostStol")]
        public IActionResult PostStol([FromBody]Stol noviStol)
        {
            int rez = vlasnik.dodajStol(noviStol);
            if (rez == 0)
                return Json(new { result = "neuspjesno" });
            else
                return Json(new { result = "uspjesno" });
        }
        [HttpPost]
        [Route("api/Vlasnik/StolKapacitet")]
        public IActionResult StolKapacitet([FromBody]Stol noviStol)
        {

            vlasnik.promijeniKapacitet(noviStol);
            return Ok();

        }
        [HttpGet]
        [Route("api/Vlasnik/GetStol/{sifra}")]
        public IActionResult GetStol(int sifra)
        {
            using (var baza = new SjediPaJediContext())
            {
                var stol = baza.Stol.Where(s => s.Sifra == sifra).First();
                return Ok(stol);
            }
        }
        [HttpGet]
        [Route("api/Vlasnik/GetStolove/{oib}")]
        public IActionResult GetStolove(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                var stolovi = baza.Stol.Where(s => s.Oibrestorana == oib).ToList();
                return Ok(stolovi);
            }
        }




        [HttpDelete]
        [Route("api/Vlasnik/DeleteKategorija/{id}")]
        public IActionResult DeleteKategorija(int id)
        {
            vlasnik.obrisiKategorija(id);
            return Ok();

        }
        [HttpPost]
        [Route("api/Vlasnik/PostKategorija")]
        public IActionResult PostKategorija([FromBody]Kategorija novaKategorija)
        {
            int rez = vlasnik.dodajKategoriju(novaKategorija);
            if (rez == 0)
                return Json(new { result = "neuspjesno" });
            else
                return Json(new { result = "uspjesno" });
        }
        [HttpPost]
        [Route("api/Vlasnik/PromjeniKategoriju")]
        public IActionResult PromjeniKategoriju([FromBody]Kategorija novaKategorija)
        {

            vlasnik.promijeniKategoriju(novaKategorija);
            return Ok();

        }

        [HttpGet]
        [Route("api/Vlasnik/GetKategorija/{id}")]
        public IActionResult GetKategorija(int id)
        {
            using (var baza = new SjediPaJediContext())
            {
                var kategorija = baza.Kategorija.Where(s => s.Id == id).First();
                return Ok(kategorija);
            }
        }

        [HttpGet]
        [Route("api/Vlasnik/GetKategorije/{oib}")]
        public IActionResult GetKategorije(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                var kategorije = baza.Kategorija.Where(s => s.Oibrestorana == oib).ToList();
                return Ok(kategorije);
            }
        }


        [HttpGet]
        [Route("api/Vlasnik/GetOsvrte/{oib}")]
        public IActionResult GetOsvrte(int oib)
        {
            using (var baza = new SjediPaJediContext())
            {
                var osvrti = baza.Osvrt.Where(o => o.Oibrestorana == oib).ToList();
                return Ok(osvrti);
            }
        }

        [HttpGet]
        [Route("api/Vlasnik/GetOsvrt/{id}")]
        public IActionResult GetOsvrt(int id)
        {
            using (var baza = new SjediPaJediContext())
            {
                var osvrt = baza.Osvrt.Where(o => o.Id == id).First();
                return Ok(osvrt);
            }
        }
        [HttpPost]
        [Route("api/Vlasnik/OdgovorOsvrta")]
        public IActionResult OdgovorOsvrta([FromBody]Osvrt osvrt)
        {
             vlasnik.OdgovoriNaOsvrt(osvrt);
            return Ok();
        }

        [HttpGet]
        [Route("api/Vlasnik/GetKategorijeSJelima/{oib}")]
        public IActionResult GetKategorijeSJelima (int oib)
        {
            List<KategorijaZaJelovnikDTO> kategorijeDTO = new List<KategorijaZaJelovnikDTO>();
            List<JeloVlasnikDTO> jelaDTO;
            using (var baza = new SjediPaJediContext())
            {
                var kategorije = baza.Kategorija.Where(k => k.Oibrestorana == oib).ToList();
                foreach(var kategorija in kategorije)
                {
                    jelaDTO = new List<JeloVlasnikDTO>();
                    KategorijaZaJelovnikDTO kategorijaDTO = new KategorijaZaJelovnikDTO();
                    kategorijaDTO.Naziv = kategorija.Naziv;
                    kategorijaDTO.Id = kategorija.Id;
                    var jela = baza.Jelo.Where(j => j.Idkategorije == kategorija.Id);
                    foreach(var jelo in jela)
                    {
                        JeloVlasnikDTO jeloDTO = new JeloVlasnikDTO();
                        jeloDTO.Id = jelo.Id;
                        jeloDTO.Naziv = jelo.Naziv;
                        jeloDTO.Opis = jelo.Opis;
                        jeloDTO.Cijena = jelo.Cijena;
                        jeloDTO.Slika = jelo.Slika;
                        jelaDTO.Add(jeloDTO);
                    }
                    kategorijaDTO.Jelo = jelaDTO;
                    kategorijeDTO.Add(kategorijaDTO);
                }

                return Ok(kategorijeDTO);
            }
        }

        [HttpPost]
        [Route("api/Vlasnik/PostJelo")]
        public IActionResult PostKategorija([FromBody]Jelo novoJelo)
        {
            int rez = vlasnik.dodajJelo(novoJelo);
            if (rez == 0)
                return Json(new { result = "neuspjesno" });
            else
                return Json(new { result = "uspjesno" });
        }
        [HttpGet]
        [Route("api/Vlasnik/GetJelo/{id}")]
        public IActionResult GetJelo(int id)
        {
            using (var baza = new SjediPaJediContext())
            {
                var jelo = baza.Jelo.Where(j => j.Id == id).First();
                return Ok(jelo);
            }
        }
        [HttpPost]
        [Route("api/Vlasnik/PromjeniJelo")]
        public IActionResult PromjeniJelo([FromBody]Jelo jelo)
        {
            vlasnik.promijeniJelo(jelo);
            return Ok();
        }
        [HttpDelete]
        [Route("api/Vlasnik/DeleteJelo/{id}")]
        public IActionResult DeleteJelo(int id)
        {
            vlasnik.obrisiJelo(id);
            return Ok();

        }

        [HttpGet]
        [Route("api/Vlasnik/GetRezervacije/{oib}")]
        public IActionResult GetRezervacije(int oib)
        {
            var rezervacije=vlasnik.pregledajRezervacijeRestorana(oib);
            return Ok(rezervacije);
        }
    }
}
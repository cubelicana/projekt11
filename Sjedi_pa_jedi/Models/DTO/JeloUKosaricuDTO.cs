﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class JeloUKosaricuDTO
    {
        public int id { get; set; }
        public int kolicina { get; set; }
    }
}

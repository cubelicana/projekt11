﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SjediPaJedi.Models;
using SjediPaJedi.Models.DTO;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SjediPaJedi.Controllers
{
    
    public class AdminController : Controller
    {
        Administrator admin = new Administrator();

        [HttpGet]
        [Route("api/Admin/GetKorisnici")]
        public IActionResult GetKorisnici()
        {
            using (var baza = new SjediPaJediContext())
            {
                

                    var korisnici = baza.Korisnik.ToList();
                    List<KorisnikDTO> lista = new List<KorisnikDTO>();
                    foreach (var korisnik in korisnici)
                    {
                        if (korisnik.KorisnickoIme != "Admin")
                        {
                        KorisnikDTO korisnikDTO = new KorisnikDTO();
                        korisnikDTO.Ime = korisnik.Ime;
                        korisnikDTO.KorisnickoIme = korisnik.KorisnickoIme;
                        korisnikDTO.Prezime = korisnik.Prezime;
                        korisnikDTO.RazinaOvlasti = korisnik.RazinaOvlasti;
                        korisnikDTO.Email = korisnik.Email;
                            lista.Add(korisnikDTO);
                        }
                    }
                    return Ok(lista);
                
            }
        }
        [HttpGet]
        [Route("api/Admin/GetVlasnike")]
        public IActionResult GetVlasnike()
        {
            using (var baza = new SjediPaJediContext())
            {
                List<VlasnikDTO> lista = new List<VlasnikDTO>();
                var vlasnici = baza.Korisnik.Where(k => k.RazinaOvlasti == 3).ToList();
                foreach(var korisnik in vlasnici)
                {
                    VlasnikDTO vlasnik = new VlasnikDTO();
                    vlasnik.KorisnickoIme = korisnik.KorisnickoIme;
                    lista.Add(vlasnik);

                }
                return Ok(lista);
            }
        }
        [HttpGet]
        [Route("api/Admin/GetOsvrte")]
        public IActionResult GetOsvrte()
        {
            using (var baza = new SjediPaJediContext())
            {
                var osvrti = baza.Osvrt.ToList();
                return Ok(osvrti);
            }
        }

        [HttpDelete]
        [Route("api/Admin/DeleteOsvrt/{id}")]
        public IActionResult DeleteOsvrt(int id)
        {
            admin.obrisiRecenzije(id);
            return Ok();
            
        }

        [HttpGet]
        [Route("api/Admin/GetRestorane")]
        public IActionResult GetRestorane()
        {
            using (var baza = new SjediPaJediContext())
            {
                var restorani = baza.Restoran.ToList();
                return Ok(restorani);
            }
        }

        [HttpPost]
        [Route("api/Admin/PostRestoran")]
        public IActionResult PostRestoran([FromBody]RestoranSVlasnikomDTO noviRestoran)
        {
            int rez = admin.dodajRestoran(noviRestoran);
            if (rez == 0)
                return Json(new { result = "neuspjesno" });
            else
                return Json(new { result = "uspjesno" });
        }

        [HttpDelete]
        [Route("api/Admin/DeleteRestoran/{oib}")]
        public IActionResult DeleteRestoran(int oib)
        {
            admin.obrisiRestoran(oib);
            return Ok();
            
        }

        [HttpPost]
        [Route("api/Admin/KorisnikIzmjene")]
        public IActionResult KorisnikIzmjene([FromBody]KorisnikDTO korisnik)
        {
            
            admin.promijeniPravaPristupa(korisnik);
            return Ok();
            
        }


        [HttpDelete]
        [Route("api/Admin/Delete/{kIme}")]
        public IActionResult Delete(string kIme)
        {
            
            admin.obrisiKorisnike(kIme);
            return Ok();
        }

        [HttpGet]
        [Route("api/Admin/Izmijeni/{kIme}")]
        public IActionResult Izmijeni(string kIme)
        {
            using (var baza = new SjediPaJediContext())
            {
                var korisnik = baza.Korisnik.First(k => k.KorisnickoIme == kIme);
                KorisnikDTO korisnikDTO = new KorisnikDTO();
                korisnikDTO.KorisnickoIme = korisnik.KorisnickoIme;
                korisnikDTO.Ime = korisnik.Ime;
                korisnikDTO.Prezime = korisnik.Prezime;
                korisnikDTO.RazinaOvlasti = korisnik.RazinaOvlasti;
                korisnikDTO.Email = korisnik.Email;
                return Ok(korisnikDTO);
            }
        }

        [HttpGet]
        [Route("api/Admin/GetStatistika")]
        public IActionResult GetStatistika()
        {
            int brojacNarudzbi = 0;
            int brojacRezervacija = 0;
            Statistika statistika = new Statistika();
            List<RestoranStatistika> listaRestorana = new List<RestoranStatistika>();
            List<KlijentStatistika> listaKlijenti = new List<KlijentStatistika>();
            using (var baza = new SjediPaJediContext())
            {
                if(baza.Restoran.Any())
                {
                    var restorani = baza.Restoran.ToList();
                    foreach(var restoran in restorani)
                    {
                        RestoranStatistika restoranStatistika = new RestoranStatistika();
                        if(baza.Kategorija.Any(k=>k.Oibrestorana==restoran.Oib))
                        {
                            var kategorije = baza.Kategorija.Where(k => k.Oibrestorana == restoran.Oib).ToList();
                            foreach(var kategorija in kategorije)
                            {
                                if(baza.Jelo.Any(j=>j.Idkategorije == kategorija.Id))
                                {
                                    var jela = baza.Jelo.Where(j => j.Idkategorije == kategorija.Id).ToList();
                                    foreach(var jelo in jela)
                                    {
                                        if(baza.Narudzba.Any(n=>n.Idjela == jelo.Id))
                                        {
                                            brojacNarudzbi += baza.Narudzba.Count(n => n.Idjela == jelo.Id);
                                        }
                                    }
                                }
                            }
                        }
                        restoranStatistika.brojNarudzbi = brojacNarudzbi;
                        brojacNarudzbi = 0;
                        
                        if(baza.Stol.Any(s=>s.Oibrestorana == restoran.Oib))
                        {
                            var stolovi = baza.Stol.Where(s => s.Oibrestorana == restoran.Oib).ToList();
                            foreach(var stol in stolovi)
                            {
                                if(baza.Rezervacija.Any(r=>r.SifraStola == stol.Sifra))
                                {
                                    brojacRezervacija += baza.Rezervacija.Count(r => r.SifraStola == stol.Sifra);
                                }
                            }
                        }
                        restoranStatistika.brojRezervacija = brojacRezervacija;
                        restoranStatistika.ime = restoran.Ime;
                        
                        listaRestorana.Add(restoranStatistika);
                        brojacRezervacija = 0;
                    }
                }
                brojacRezervacija = 0;
                brojacNarudzbi = 0;
                if(baza.Korisnik.Any(k=>k.KorisnickoIme!="Admin"))
                {
                    var korisnici = baza.Korisnik.Where(k => k.KorisnickoIme != "Admin").ToList();
                    foreach(var korisnik in korisnici)
                    {
                        KlijentStatistika klijentDTO = new KlijentStatistika();
                        if(baza.Narudzba.Any(n=>n.KorisnickoIme == korisnik.KorisnickoIme))
                        {
                            brojacNarudzbi += baza.Narudzba.Count(n => n.KorisnickoIme == korisnik.KorisnickoIme);
                        }
                        klijentDTO.KorisnickoIme = korisnik.KorisnickoIme;
                        klijentDTO.brojNarudžbi = brojacNarudzbi;
                        brojacNarudzbi = 0;

                        if(baza.Rezervacija.Any(r => r.KorisnickoIme == korisnik.KorisnickoIme))
                        {
                            brojacRezervacija += baza.Rezervacija.Count(r => r.KorisnickoIme == korisnik.KorisnickoIme);
                        }
                        klijentDTO.brojRezervacija = brojacRezervacija;
                        brojacRezervacija = 0;
                        listaKlijenti.Add(klijentDTO);


                    }
                        
                }
                statistika.klijenti = listaKlijenti;
                statistika.restorani = listaRestorana;
                return Ok(statistika);
            }
        }

    }
}

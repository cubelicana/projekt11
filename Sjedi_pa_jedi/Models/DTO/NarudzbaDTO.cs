﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SjediPaJedi.Models.DTO
{
    public class NarudzbaDTO
    {
        public int Id { get; set; }
        public TimeSpan Vrijeme { get; set; }
        public string jelo { get; set; }
        public string KorisnickoIme { get; set; }
    }
}

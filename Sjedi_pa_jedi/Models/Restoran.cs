﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Restoran
    {
        public Restoran()
        {
            Kategorija = new HashSet<Kategorija>();
            Osvrt = new HashSet<Osvrt>();
            Stol = new HashSet<Stol>();
        }

        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public string Fax { get; set; }
        public int Oib { get; set; }
        public string Iban { get; set; }
        public string ZiroRacun { get; set; }
        public string Slika { get; set; }

        public virtual ICollection<Kategorija> Kategorija { get; set; }
        public virtual ICollection<Osvrt> Osvrt { get; set; }
        public virtual ICollection<Stol> Stol { get; set; }
    }
}

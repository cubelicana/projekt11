﻿import React from 'react';
import { Card, CardTitle, CardImg, CardText, Button, CardBody, ListGroupItemText, ListGroupItemHeading, CardSubtitle, ListGroup, ListGroupItem, Container, Row, Col } from 'reactstrap';
import { KlijentNavBar } from './NavBarBezRestorana';

export class KlijentRestoran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restoran: [],
            kategorije: [],
            osvrt: "",
            kolicina: 1,
            ima: false,
            kosara: [],
            zahtjev: true,
            ponovno:false




        };
        
        var oib = this.props.match.params.oib;
        fetch("api/Home/Restoran/" + oib)
            .then(response => response.json())
            .then(restorann => {
                if (restorann.osvrt == null)
                    this.setState({
                        restoran: restorann,
                        kategorije: restorann.kategorije,

                    });
                else
                    this.setState({
                        restoran: restorann,
                        kategorije: restorann.kategorije,
                        osvrt: restorann.osvrt,

                    });

            });
        this.Kolicina = this.Kolicina.bind(this);
        this.pregledajNarudzbu = this.pregledajNarudzbu.bind(this);
        this.renderKosara = this.renderKosara.bind(this);
    }

    Osvrti(oib) {
        
        this.props.history.push("/klijentRecenzije/" + oib);
    }

    renderOsvrt() {
        if (this.state.osvrt != '')
            return (
                <Card>
                    <CardBody>
                        <CardTitle>{this.state.osvrt.korisnickoIme}</CardTitle>
                        <CardSubtitle>Ocjena:&nbsp;{this.state.osvrt.ocjena}</CardSubtitle>
                        <CardText>Opis:&nbsp;{this.state.osvrt.opis}<br />Odgovor:&nbsp;{this.state.osvrt.odgovor}</CardText>
                    </CardBody>
                </Card>
                

            );
    }
    renderBotun() {
        var oib = this.props.match.params.oib;
        if (this.state.osvrt != '')
            return (
                <Button color="warning" onClick={(kIme) => this.Osvrti(oib)}>Pogledaj sve osvrte</Button>
                );
    }

    renderSlika(slika) {
        if (slika == null)
            return (<span />);
        else
            return (<img src={slika} width="75" height="75" />);
    }

    dodajUKosaricu(id,naziv,cijena) {
        var podaci = {
            'id': id,
            'kolicina': this.state.kolicina
        };
        cijena = this.state.kolicina * cijena;
        var podaciZaKosaru = {
            'id':id,
            'naziv': naziv,
            'kolicina': this.state.kolicina,
            'cijena': cijena
        };

       
       

        fetch('api/Klijent/PostJelo', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(podaci)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno') {

                    var kosarica = this.state.kosara;

                    kosarica.push(podaciZaKosaru);
                    this.setState({
                        kolicina: 1,
                        ima: true,
                        kosara: kosarica,
                        zahtjev: false

                    });
                }
                else if (findresponse.result == 'jeloPostoji') {
                    this.setState({
                        ponovno:true
                    });
                    this.renderKosara();
                }
            })
    }

    Kolicina(e) {
        this.setState({ kolicina: e.target.value });
    }

    renderSelect() {
        return (
            <select name="kolicina" onChange={this.Kolicina}>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
            );
    }
    pregledajNarudzbu() {
        var oib = this.props.match.params.oib;
        this.setState({ zahtjev: true });
        this.props.history.push("/klijentNarudzba/"+oib);
    }
    renderBotunKosara() {
        if (this.state.ima == false)
            return (<br />)
        else if (this.state.ima == true)
            return (<Button color="success" onClick={this.pregledajNarudzbu}>Pregledaj narudžbu</Button>);

    }
    renderKosara() {
        if ((this.state.kosara.length == 0 && this.state.zahtjev == true && this.state.ima == false) || (this.state.ponovno==true)) {
            fetch("api/Klijent/GetJela")
                .then(response => response.json())
                .then(jelaa => {
                    if (jelaa.result == 'nema')
                        this.setState({
                            kosara: [],
                            ima: false,
                            zahtjev: false,
                            ponovno:false

                        });
                    else
                        this.setState({
                            kosara: jelaa,
                            ima: true,
                            zahtjev: false,
                            ponovno:false

                        });
                    this.renderBotunKosara();
                })
        }
        return (
            <ListGroup>
                {this.state.kosara.map(jelo =>
                    <ListGroupItem key={jelo.id}>
                        <ListGroupItemText>{jelo.kolicina}x&nbsp;{jelo.naziv}&nbsp;Cijena:&nbsp;{jelo.cijena}kn&nbsp;
                                            <Button color="danger" onClick={(kIme) =>  this.izbrisi(jelo.id) }>X</Button>
                        </ListGroupItemText>
                    </ListGroupItem>
                )}
            </ListGroup>
            );
    }

    izbrisi(id) {
        fetch('api/Klijent/IzbrisiIzKosarice/' + id, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    kosara: this.state.kosara.filter((rec) => {
                        return (rec.id != id);
                    })
                })
            if (this.state.kosara.length == 0) {
                this.setState({ ima: false });
                this.renderBotunKosara();
            }


        })
    }

    render() {

        return (

            <div>
                <KlijentNavBar />
                <Container>
                    <Row>
                        <Col xs="3">
                            <CardImg src={this.state.restoran.slika} width="100%" />
                            <Card body inverse color="info">

                                <CardBody>

                                    <CardTitle>Naziv:&nbsp;{this.state.restoran.ime}</CardTitle>
                                    <CardSubtitle>Adresa:&nbsp;{this.state.restoran.adresa}</CardSubtitle>
                                    <CardText>OIB:&nbsp;{this.state.restoran.oib}<br />Telefon:&nbsp;{this.state.restoran.telefon}<br />Iban:&nbsp;{this.state.restoran.iban}
                                        <br />Fax:&nbsp;{this.state.restoran.fax}<br />Ziro racun:&nbsp;{this.state.restoran.ziroRacun}<br /></CardText>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col>
                            {this.state.kategorije.map(kategorija =>
                                <ListGroup key={kategorija.naziv}>
                                    <ListGroupItem active>{kategorija.naziv}</ListGroupItem>
                                    {kategorija.jelo.map(jelo =>
                                        <ListGroupItem key={jelo.id}>
                                            <ListGroupItemHeading>{jelo.naziv}</ListGroupItemHeading>
                                            <ListGroupItemText>{this.renderSlika(jelo.slika)}&nbsp;<b>Opis:</b>&nbsp;{jelo.opis}&nbsp;<b>Cijena:</b>&nbsp;{jelo.cijena}&nbsp;
                                                Količina:&nbsp;{this.renderSelect()}<br />
                                                <Button color="success" onClick={(kIme) => this.dodajUKosaricu(jelo.id,jelo.naziv,jelo.cijena)}>Dodaj u košaricu</Button>
                                            </ListGroupItemText>
                                        </ListGroupItem>
                                    )}
                                </ListGroup>
                                
                            )}

                        </Col>
                        <Col xs="3">
                           <div> {this.renderOsvrt()}
                            {this.renderBotun()}
                            <h2>Moja narudžba</h2>
                            {this.renderKosara()}
                            {this.renderBotunKosara()}</div>


                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
﻿using System;
using System.Collections.Generic;

namespace SjediPaJedi.Models
{
    public partial class Jelo
    {
        public Jelo()
        {
            Narudzba = new HashSet<Narudzba>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public decimal Cijena { get; set; }
        public int Idkategorije { get; set; }
        public string Slika { get; set; }

        public virtual Kategorija IdkategorijeNavigation { get; set; }
        public virtual ICollection<Narudzba> Narudzba { get; set; }
    }
}

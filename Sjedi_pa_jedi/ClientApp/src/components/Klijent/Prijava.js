﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { MapContainerZaPrijavu } from './MapaZaPrijavu';



export class Prijava extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            korisnickoIme: '',
            lozinka: '',
            uloga:'Klijent'
        };
        this.korisnickoIme = this.korisnickoIme.bind(this);
        this.lozinka = this.lozinka.bind(this);
        this.napraviPrijavu = this.napraviPrijavu.bind(this);
        this.napraviRegistraciju = this.napraviRegistraciju.bind(this);
        this.uloga = this.uloga.bind(this);
    }

    korisnickoIme(event) {
        this.setState({ korisnickoIme: event.target.value });
    }
    lozinka(event) {
        this.setState({ lozinka: event.target.value });
    }

    napraviPrijavu(event) {
        
        fetch('api/Home/prijava', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'Klijent')
                    this.props.history.push("/mapa");
                else if (findresponse.result == 'Administrator')
                    this.props.history.push("/adminKorisnici");
                else if (findresponse.result == 'Vlasnik')
                    this.props.history.push("/vlasnikRestorani");
                else if (findresponse.result == 'Zaposlenik')
                    this.props.history.push("/zaposlenikAktivneNarudzbe");
                else if (findresponse.result == 'nemaOvlasti')
                    alert("Greška! Nemate odabrane ovlasti");
                else
                    alert('Greška! Neuspješna prijava');
            })
    }

    napraviRegistraciju() {
        this.props.history.push("/registracija");
    }
    uloga(event) {
        this.setState({ uloga: event.target.value });
    }

    render() {
        return (
            <Container>
                <Row>
                    
                        <Col xs="3">
            <Form>
                <FormGroup row>
                    <Label for="kIme">Korisnicko ime</Label>
                    <Input type="text" onChange={this.korisnickoIme} id="kIme" value={this.state.korisnickoIme} required />
                </FormGroup>
                
                <FormGroup row>
                    <Label for="lozinka">Lozinka</Label>
                     <Input type="password" onChange={this.lozinka} id="lozinka" value={this.state.lozinka} required/>
                </FormGroup>
                <FormGroup row>
                                <Input type="select"  onChange={this.uloga}>
                                    <option value="Klijent">Klijent</option>
                                    <option value="Zaposlenik">Zaposlenik</option>
                                    <option value="Vlasnik">Vlasnik</option>
                                    <option value="Administrator">Administrator</option>
                                    
                                </Input>

                </FormGroup>
 
                <FormGroup row>
                    <Button color="primary" id="Prijava" onClick={this.napraviPrijavu}>Prijava</Button>
                    &nbsp;
                    <Button color="success" id="Registracija"  onClick={this.napraviRegistraciju}>Registracija</Button>

                </FormGroup>
                        </Form>
                    </Col>
                    <Col>
                        <MapContainerZaPrijavu google={window.google} />
                    </Col>
                </Row>
            </Container>
           
        );
    }
}
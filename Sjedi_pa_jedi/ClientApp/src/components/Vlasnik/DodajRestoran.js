﻿import React from 'react';
import FileBase64 from 'react-file-base64';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

export class VlasnikDodajRestoran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ime: '',
            adresa: '',
            telefon: '',
            fax: '',
            iban: '',
            ziroRacun: '',
            oib: '',
            slika:''
        };

        this.ime = this.ime.bind(this);
        this.adresa = this.adresa.bind(this);
        this.telefon = this.telefon.bind(this);
        this.fax = this.fax.bind(this);
        this.iban = this.iban.bind(this);
        this.ziroRacun = this.ziroRacun.bind(this);
        this.oib = this.oib.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);

    }


    ime(event) {
        this.setState({ ime: event.target.value });
    }
    adresa(event) {
        this.setState({ adresa: event.target.value });
    }
    telefon(event) {
        this.setState({ telefon: event.target.value });
    }
    fax(event) {
        this.setState({ fax: event.target.value });
    }
    iban(event) {
        this.setState({ iban: event.target.value });
    }
    ziroRacun(event) {
        this.setState({ ziroRacun: event.target.value });
    }
    oib(event) {
        this.setState({ oib: event.target.value });
    }
    getFiles(files) {

        this.setState({ slika: files });
        var slika = this.state.slika.base64;
        this.setState({ slika: slika });

    }
    napraviSubmit(event) {
        event.preventDefault();
        fetch('api/Vlasnik/PostRestoran', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/vlasnikRestorani");
                else
                    alert('Greška! Restoran već postoji');
            })

    }
    napraviCancel() {
        
        this.props.history.push("/vlasnikRestorani");
    }

    render() {
        return (
            <div>
                
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                            <Form>

                                <FormGroup row>
                                    <Label for="ime">Ime</Label>
                                    <Input type="text" id="ime" onChange={this.ime} value={this.state.ime} required />
                                </FormGroup>


                                <FormGroup row>

                                    <Label for="adresa">Adresa</Label>
                                    <Input type="text" id="adresa" onChange={this.adresa} value={this.state.adresa} required />

                                </FormGroup>

                                <FormGroup row>
                                    <Label for="telefon">Telefon</Label>
                                    <Input type="text" id="telefon" onChange={this.telefon} value={this.state.telefon} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="fax">Fax</Label>
                                    <Input type="text" id="fax" onChange={this.fax} value={this.state.fax} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="iban">Iban</Label>
                                    <Input type="text" onChange={this.iban} id="iban" value={this.state.iban} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="ziroRacun">Ziro racun</Label>
                                    <Input type="text" onChange={this.ziroRacun} id="ziroRacun" value={this.state.ziroRacun} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="oib">OIB</Label>
                                    <Input type="text" onChange={this.oib} id="oib" value={this.state.oib} required />
                                </FormGroup>

                                <FormGroup row>
                                    <Label for="slika">Slika:&nbsp;</Label>
                                    <FileBase64 onDone={this.getFiles.bind(this)} />
                                </FormGroup>

                                <FormGroup row>
                                    <Button color="primary" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
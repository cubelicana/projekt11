﻿import React from 'react';
import { AdminNavBar } from './NavBar';
import { Table, Button, Container, Row, Col } from 'reactstrap';




export class Statistika extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
            restorani: [],
            klijenti:[]
        };


        fetch('api/Admin/GetStatistika')
            .then(response => response.json())
            .then(statistikaa => {
                this.setState({
                    restorani: statistikaa.restorani,
                    klijenti:statistikaa.klijenti
                });
            });
       
    }




    render() {
        return (
            <div>
                <AdminNavBar />
                <Container>
                    <Row>
                        <Col>
                            <h2>Restorani</h2>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>Ime</th>
                                        <th>Broj narudžbi</th>
                                        <th>Broj rezervacija</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.restorani.map(restoran =>
                                        <tr key={restoran.ime}>
                                            <td>{restoran.ime}</td>
                                            <td>{restoran.brojNarudzbi}</td>
                                            <td>{restoran.brojRezervacija}</td>                                          
                                        </tr>

                                    )}
                                </tbody>
                            </Table>
                        </Col>
                        <Col>
                            <h2>Klijenti</h2>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>Ime</th>
                                        <th>Broj narudžbi</th>
                                        <th>Broj rezervacija</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.klijenti.map(klijent =>
                                        <tr key={klijent.korisnickoIme}>
                                            <td>{klijent.korisnickoIme}</td>
                                            <td>{klijent.brojNarudzbi}</td>
                                            <td>{klijent.brojRezervacija}</td>
                                        </tr>

                                    )}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    
                </Container>
            </div>

        );
    }
}
﻿import React from 'react';
import { AdminNavBar } from './NavBar';
import { Card, Container,Row,Col, CardText, CardBody,CardTitle, CardSubtitle, Button} from 'reactstrap';



export class AdminOsvrti extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            osvrti: []
            
        };


        fetch('api/Admin/GetOsvrte')
            .then(response => response.json())
            .then(osvrti => {
                this.setState({ osvrti });
            });
        this.handleDelete = this.handleDelete.bind(this);

        
    }



    handleDelete(id) {

        fetch('api/Admin/DeleteOsvrt/' + id, {
            method: 'delete'
        }).then(data => {
            this.setState(
                {
                    osvrti: this.state.osvrti.filter((rec) => {
                        return (rec.id != id);
                    })
                })

        })

    }

    


    render() {
        return (
            <div>
                <AdminNavBar />
                <Container>
                    <Row>
                        <Col sm="12" md={{ size: 6, offset: 3 }}>
                {this.state.osvrti.map(osvrt =>
                <Card>
                    <CardBody>
                       
                            <CardTitle>Korisnik:&nbsp; {osvrt.korisnickoIme}</CardTitle>
                            <CardSubtitle>Restoran:&nbsp; {osvrt.oibrestorana}</CardSubtitle>
                            <CardText>Ocjena:&nbsp;{osvrt.ocjena}</CardText>
                            <CardText>Opis:&nbsp; {osvrt.opis}</CardText>
                            <CardText>Odgovor:&nbsp; {osvrt.odgovor}</CardText>
                            <Button color="danger" onClick={(kIme) => { if (window.confirm('Jesi li siguran da želiš izbrisati ovaj osvrt?')) this.handleDelete(osvrt.id) }}>Izbriši</Button>
                       
           
                    </CardBody>
                    </Card>
                            )}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
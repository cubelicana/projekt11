﻿import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { VlasnikNavBar } from './NavBar';

export class VlasnikDodajZaposlenika extends React.Component {
    constructor(props) {
        super(props);
       
        this.state = {
            KorisnickoIme: '',
            OibRestorana: '',
            RazinaOvlasti: 2,
            
        };
        

        this.korisnickoIme = this.korisnickoIme.bind(this);
        this.napraviSubmit = this.napraviSubmit.bind(this);
        this.napraviCancel = this.napraviCancel.bind(this);
        
    }

    componentDidMount() {
        var oib = this.props.match.params.oib;
        this.setState({ OibRestorana: oib });
    }

    korisnickoIme(event) {
        this.setState({ KorisnickoIme: event.target.value });
        
    }
   

    napraviSubmit(event) {
        
        event.preventDefault();
        fetch('api/Vlasnik/PostZaposlenik', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then((Response) => Response.json())
            .then((findresponse) => {
                if (findresponse.result == 'uspjesno')
                    this.props.history.push("/vlasnikZaposlenici/" + this.state.OibRestorana);
                else
                    alert('Greška! Korisnika nije moguće dodati kao zaposlenika');
            })




    }
    napraviCancel() {
        var oib = this.props.match.params.oib;
        this.props.history.push("/vlasnikZaposlenici/" + oib);
    }
    render() {
        var oib = this.props.match.params.oib;
        return (
            <div>
            <VlasnikNavBar oibRestorana={oib} />
            <Container>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <Form>
                            <FormGroup row>
                                <Label for="korisnickoIme">Korisnicko ime</Label>
                                <Input type="text" onChange={this.korisnickoIme} id="korisnickoIme" value={this.state.KorisnickoIme} required />
                            </FormGroup>


                            <FormGroup>
                                    <Button color="primary" id="submit" onClick={this.napraviSubmit}>Dodaj</Button>
                                    &nbsp;
                                    <Button color="warning" id="cancel" onClick={this.napraviCancel}>Odustani</Button>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
                </Container>
            </div>

        );
    }
}